<?php

namespace App\Http\Controllers;

use App\Services\EmailServices\SendEmailService;
use App\Services\SearchDBStorageService;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Session;

class LandingPageController extends Controller
{
    const MIN_PERMITTED_KEYWORDS = 3;

    public function getLandingPage(){
        if(\Auth::user() == null) {
            return view('landing_page');
        } else {
            return redirect('display-searches');
        }
    }

    public function postLandingPageSearchFromKeywords(Request $request){
        $requestParameters = $request->all();
        if(isset($requestParameters['keyword'])) {
            // get the specified search location
            $locationSpecified = null;
            if($requestParameters['location_search'] != "0") {
                $locationSpecified = $requestParameters['country'];
            }
            $keywords = $requestParameters['keyword'];
            $keywordsCount = count($keywords);
            if ($keywordsCount >= self::MIN_PERMITTED_KEYWORDS) {
                $searchDBStorageService = new SearchDBStorageService();
                $search = $searchDBStorageService->storeSearchToDB($keywords, $locationSpecified);
                Session::put('search_id', $search['search_id']);
                return $keywordsCount;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public function contactUs(Request $request){
        $sendContactUsEmailService = new SendEmailService();
        return $sendContactUsEmailService->sendRawEmailToApp($request->all()['email'],
            $request->all()['subject'], $request->all()['message']);
    }
}
