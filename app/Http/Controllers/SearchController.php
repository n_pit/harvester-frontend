<?php

namespace App\Http\Controllers;

use App\Models\Search;
use App\Services\ExportFile;
use App\Services\PermissionsToDisplaySearch\CheckUsersPermissions;
use App\Services\SearchPerformingService;
use App\Services\SearchDisplayService;
use Illuminate\Http\Request;
use App\Http\Requests;

class SearchController extends Controller{

    private $searchDisplayService;
    private $searchPerformingService;

    public function __construct(){
        // make sure that this is a logged in user
        $this->middleware('auth');
        $this->searchDisplayService = new SearchDisplayService();
        $this->searchPerformingService = new SearchPerformingService();
    }

    // display a single search
    public function getDisplaySearch($search_id){
        $checkUsersPermissions = new CheckUsersPermissions();
        if($checkUsersPermissions->searchBelongsToLoggedInUserAndIsCompleted($search_id)) {
            $search = array('search_name' => null, 'keywords' => array());
            if (isset($search_id)) {
                $searchFromDB = Search::find($search_id);
                if ($searchFromDB->got_contacts_found == 1) {
                    $temp = $this->searchDisplayService->getSearchInfoAndSearchResults($search_id);
                } else {
                    $temp = $this->searchPerformingService->fetchContactsFromApiStoreThemAndRetrieveThem($search_id);
                }
                if ($temp != null) {
                    $search = $temp;
                }
            }
            return view('searches.display_search')
                ->with('search', $search)
                ->with('displayMenu', 'no');
        } else {
            return redirect('display-searches');
        }
    }

    // display all searches
    public function getDisplayAllSearches(){
        $allSearches = $this->searchDisplayService->getAllUserSearchesInfo();
        $showSearchingAlert = \Session::get('showSearchingAlert', function(){
            return "hide";
        });
        $showSearchingError = \Session::get('showSearchingError', function(){
            return "hide";
        });
        return view('searches.all_searches')
            ->with('allSearches', $allSearches)
            ->with('displayMenu', 'yes')
            ->with('showSearchingAlert', $showSearchingAlert)
            ->with('showSearchingError', $showSearchingError);
    }

    public function postEditNameSearch(Request $request){
        $this->searchDisplayService->editSavedSearchName($request);
    }

    public function getNewSearch(){
        return view('searches.new-search')
            ->with('displayMenu', 'yes');
    }

     public function postSearchByKeywords(Request $request){
         $locationSpecified = null;
         if($request->location_search != "0") {
             $locationSpecified = $request->country;
         }
         if($this->searchPerformingService->performSearchFromKeywordsAndStoreIt($request->keywords,
                 $locationSpecified) == null){
             return redirect('display-searches')
                 ->with('showSearchingError', '');
         }
         return redirect('display-searches')
             ->with('showSearchingAlert', '');
    }

    // TODO: TO BE IMPLEMENTED
    public function postSearchByKeywordsExtractedFromURLs(Request $request){
//        $client = new Client('http://192.168.1.7:8080/CHarvesterRest/');
//        $response = $client->get('extract_contacts?key='.'LcRotHSR7vxRl70EQyjPb54n2YzGK2iR'.'&userId='.'1'.'&searchId='.'1'.'&Keywords='.'keyword')->send();
//
////        dd($response->json());
//
//        $keywordsExtracted = $this->communicateWithREST->apiKeywordsExtractedFromURLs('LcRotHSR7vxRl70EQyjPb54n2YzGK2iR', 1, 'http://www.in.gr');
//        $contactsFound = $this->communicateWithREST->apiStartContactHarvestingFromKeywords('LcRotHSR7vxRl70EQyjPb54n2YzGK2iR', 1, 'keyword');
////        dd($contact);
//        return redirect('display-searches');
    }

    public function getStatusOfUser(){
        return $this->searchPerformingService->getSearchesStatusesForUserAndStoreThem();
    }

    public function getContactsFromKeywordsSearch($search_id){
        return $this->searchPerformingService->fetchContactsFromApiStoreThemAndRetrieveThem($search_id);
    }
    
    public function deleteSearch($search_id){
        // returns "true" or "false" strings
        return $this->searchPerformingService->deleteSearch($search_id);
    }

    public function cancelSearch($search_id){
        // returns "true" or "false" strings
        return $this->searchPerformingService->cancelSearch($search_id);
    }

    public function exportSearchContactsToCSV($search_id){
        $exportFile = new ExportFile();
        return $exportFile->exportContactsCSVFile($search_id);
    }
    
    public function removeContact($contact_id){
        // returns "true" or "false" strings
        return $this->searchPerformingService->removeContactFromDB($contact_id);
    }
}
