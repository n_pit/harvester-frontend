<?php

// landing page routes
Route::get('/', 'LandingPageController@getLandingPage');
// end of landing page routes

// perform search from the landing page form
Route::post('landing-page-search', 'LandingPageController@postLandingPageSearchFromKeywords');
// end of perform search from the landing page form

// view search views
Route::get('display-searches', 'SearchController@getDisplayAllSearches');
Route::get('display-search/{id}', 'SearchController@getDisplaySearch');
Route::post('edit-search', 'SearchController@postEditNameSearch');
// end of view search views

// store search results to DB
Route::get('get-contacts/{search_id}', 'SearchController@getContactsFromKeywordsSearch');
// end of store search results to DB

// get user's status of searches
Route::get('user-status', 'SearchController@getStatusOfUser');

// delete a search
Route::get('delete-search/{search_id}', 'SearchController@deleteSearch');

// cancel a search
Route::get('cancel-search/{search_id}', 'SearchController@cancelSearch');

// New Search
Route::get('new-search', 'SearchController@getNewSearch');
Route::post('new-search/search-from-keywords', 'SearchController@postSearchByKeywords');
Route::post('new-search/search-from-urls', 'SearchController@postSearchByKeywordsExtractedFromURLs');
// end of New Search

// export searches
Route::get('export-contacts-csv/{search_id}', 'SearchController@exportSearchContactsToCSV');
// end of export searches

// remove contact
Route::get('remove-contact/{contact_id}', 'SearchController@removeContact');

// contact us email
Route::get('contact-us', 'LandingPageController@contactUs');

Route::auth();

Route::get('/home', 'HomeController@index');
