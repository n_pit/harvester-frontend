<?php

namespace App\Interfaces;

use App\Services\SearchPerformingService;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use App\Services\EmailServices\SendEmailService;

trait MyAuthentication{

    use AuthenticatesAndRegistersUsers{
        login as basicLogin;
        showLoginForm as showLoginView;
        register as basicRegister;
        showRegistrationForm as showRegisterView;
    }

    public function showLoginForm(){
        $result = $this->showLoginView();
        return $result->with('tabSelected', 'login');
    }

    public function showRegistrationForm(){
        $result = $this->showRegisterView();
        return $result->with('tabSelected', 'register');
    }

    public function login(Request $request){
        $result = $this->basicLogin($request);
        $showSearchingAlert = "hide";
        // retrieve from session the search keywords if they exist
        // and store them in DB
        if (\Session::has('search_id')) {
            $searchPerformingService = new SearchPerformingService();
            $search_id = \Session::get('search_id');
            if($searchPerformingService->performSearchAfterAuthentication($search_id) == null){
                return redirect('login');
            }
            \Session::forget('search_id');
            $showSearchingAlert = "";
        }
        return $result->with('showSearchingAlert', $showSearchingAlert);
    }

    public function register(Request $request){
        $result = $this->basicRegister($request);
        $showSearchingAlert = "hide";
        // retrieve from session the search keywords if they exist
        // and store them in DB
        if(\Session::has('search_id')){
            $searchPerformingService = new SearchPerformingService();
            $search_id = \Session::get('search_id');
            if($searchPerformingService->performSearchAfterAuthentication($search_id) == null){
                return redirect('register');
            }
            $sendEmailService = new SendEmailService();
            $sendEmailService->sendNewRegisteredUserEmail();
            \Session::forget('search_id');
            $showSearchingAlert = "";
        }
        return $result->with('showSearchingAlert', $showSearchingAlert);
    }
}
