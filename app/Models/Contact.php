<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contact extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'domain', 'email', 'search_id', 'phone', 'address', 'facebookURL', 'twitterURL', 'contactId', 'relatedQueries',
    ];

    // returns the search to which it belongs
    public function search(){
        return $this->belongsTo('App\Models\Search');
    }
}
