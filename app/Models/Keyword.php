<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Keyword extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'keyword', 'search_id',
    ];

    // returns the search to which it belongs
    public function search(){
        return $this->belongsTo('App\Models\Search');
    }
}
