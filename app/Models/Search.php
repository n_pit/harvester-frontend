<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Search extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name', 'user_id', 'location'
    ];

    // returns all the search keywords
    public function keywords(){
        return $this->hasMany('App\Models\Keywords');
    }

    // returns all the search contacts found
    public function contacts(){
        return $this->hasMany('App\Models\Contacts');
    }

    // returns the user who made the search
    public function user(){
        return $this->belongsTo('App\Models\User');
    }
}
