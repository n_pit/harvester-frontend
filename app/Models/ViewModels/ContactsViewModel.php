<?php

namespace App\Models\ViewModels;

class ContactsViewModel{

    private $id = "", $domain = "", $email = "", $phone = "", $address = "", $facebookURL = "", $twitterURL = "",
        $contactId = "", $relatedQueries = "";

    public function __construct($id, $domain, $email, $phone, $address, $facebookURL, $twitterURL, $contactId,
        $relatedQueries){
        $this->id = $id;
        $this->domain = $domain;
        $this->email = $email;
        $this->phone = $phone;
        $this->address = $address;
        $this->facebookURL = $facebookURL;
        $this->twitterURL = $twitterURL;
        $this->contactId = $contactId;
        $this->relatedQueries = $relatedQueries;
    }

    public function formatDomain(){
        if(!empty($this->domain)){
            $domains = explode(" ", $this->domain);
            $tmp = array();
            foreach ($domains as $domain) {
                $url_parsed = parse_url($domain);
                if (!$url_parsed) {
                    array_push($tmp, $domain);
                } else {
                    if (empty($url_parsed['host']) and !empty($url_parsed['path']) and
                        (substr($url_parsed['path'], 0, 4) == "www." or substr($url_parsed['path'], -4, 1) == "." or
                        substr($url_parsed['path'], -3, 1) == ".") and substr($url_parsed['path'], -1, 1) != "."
                        and substr($url_parsed['path'], -1, 1) != ")") {
                        array_push($tmp, "<a href='http://" . $domain . "'>http://" .
                            $domain . "</a>");
                    } elseif (!empty($url_parsed['host']) and (substr($url_parsed['host'], 0, 4) == "www." or !empty($url_parsed['scheme']))) {
                        array_push($tmp, "<a href='" . $domain . "'>" .
                            $domain . "</a>");
                    } else {
                        array_push($tmp, $domain);
                    }
                }
            }
	        return implode(" ", $tmp);
        } else {
	        return "";
        }        
    }

    public function formatSocialMediaWithLinks(){
        $tmp = "";
        if(!empty($this->facebookURL)) {
            $tmp .= "<a href='" . $this->facebookURL . "'>" . $this->facebookURL . "</a>";
        }
        if(!empty($this->facebookURL) and !empty($this->twitterURL)){
            $tmp .= ", ";
        }
        if(!empty($this->twitterURL)) {
            $tmp .= "<a href='" . $this->twitterURL . "'>" .
                $this->twitterURL . "</a>";
        }
        return $tmp;
    }

    public function formatSocialMediaWithoutLinks(){
        $tmp = "";
        if(!empty($this->facebookURL)) {
            $tmp .= $this->facebookURL;
        }
        if(!empty($this->facebookURL) and !empty($this->twitterURL)){
            $tmp .= ", ";
        }
        if(!empty($this->twitterURL)) {
            $tmp .= $this->twitterURL;
        }
        return $tmp;
    }

    public function getId(){
        return $this->id;
    }

    public function getDomain(){
        return $this->domain;
    }

    public function getEmail(){
        return $this->email;
    }

    public function getPhone(){
        return $this->phone;
    }

    public function getAddress(){
        return $this->address;
    }

    public function getFacebookURL(){
        return $this->facebookURL;
    }

    public function getTwitterURL(){
        return $this->twitterURL;
    }

    public function getContactId(){
        return $this->contactId;
    }

    public function getRelatedQueries(){
        return $this->relatedQueries;
    }
}
