<?php

namespace App\Services;

use Guzzle\Service\Client;
use Illuminate\Support\Facades\Log;

class CommunicateWithREST{

    protected $apiKey = "LcRotHSR7vxRl70EQyjPb54n2YzGK2iR";
//    protected $restUrl = "http://charvester.scify.org/CHarvesterRest/";
    protected $restUrl = "http://192.168.1.7:8080/CHarvesterRest/";
    protected $userId = null;

    public function __construct(){
        if(\Auth::check()) {
            $this->userId = \Auth::user()->id;
        }
    }

    // TODO: to be implemented API - GET KEYWORDS FROM URLs
    public function apiKeywordsExtractedFromURLs($search_id, $urls){
        $rest_api = 'http://192.168.1.7:8080/CHarvesterRest/extract_keywords?key='.$this->apiKey.'&userId='.$this->userId.'&searchId='.$search_id.'&urls='.$urls;
        $result = json_decode(file_get_contents($rest_api, true));
        return $result;
    }

    // API - INITIALIZE CONTACT HARVESTING FROM KEYWORDS
    public function apiStartContactHarvestingFromKeywords($search_id, $keywords, $location_specified){
        try {
            $locationTemp = "";
            if($location_specified != null){
                $locationTemp = " loc:" . $location_specified;
            }
            $client = new Client($this->restUrl);
            $request = $client->post('extract_contacts', ['content-type' => 'x-www-form-urlencoded'], ['key' => $this->apiKey, 'userId' => $this->userId, 'searchId' => $search_id, 'keywords' => $keywords, 'extraStringPerSearch' => 'contacts' . $locationTemp, 'maxTopResultsPerSearch' => 20]);
            $response = $request->send();
            return json_decode($response->getBody());
        } catch(\Exception $e){
            Log::info("An error occurred while trying to 'INITIATE' harvesting with search id: " . $search_id .
                ". Exception msg: " . $e);
            return null;
        }
    }

    // cancel a pending/running harvesting contacts from keywords
    public function apiCancelContactHarvestingFromKeywords($search_id){
        try {
            $client = new Client($this->restUrl);
            $request = $client->delete('extract_contacts?key=' . $this->apiKey . '&userId=' . \Auth::user()->id . '&searchId=' . $search_id, ['content-type' => 'x-www-form-urlencoded']);
            $response = $request->send();
            return json_decode($response->getBody());
        } catch(\Exception $e){
            Log::info("An error occurred while trying to 'CANCEL' harvesting with search id: " . $search_id .
                ". Exception msg: " . $e);
            return null;
        }
    }

    // get statuses for all user's searches
    public function apiSearchStatusOfUser(){
        try {
            $client = new Client($this->restUrl);
            $request = $client->get('searchStatusOfUser', ['content-type' => 'x-www-form-urlencoded'], ['query' => ['key' => $this->apiKey, 'userId' => $this->userId]]);
            $response = $request->send();
            return json_decode($response->getBody());
        } catch(\Exception $e){
            Log::info("Could not get statuses of user's searches with id: " . \Auth::user()->id .
                ". Exception msg: " . $e);
            return null;
        }
    }

    // get contacts harvested from a keywords search
    public function apiGetContactsFromKeywordsSearch($search_id){
        try {
            $client = new Client($this->restUrl);
            $request = $client->get('extract_contacts', ['content-type' => 'x-www-form-urlencoded'], ['query' => ['key' => $this->apiKey, 'userId' => $this->userId, 'searchId' => $search_id]]);
            $response = $request->send();
            return json_decode($response->getBody());
        } catch(\Exception $e){
            Log::info("Could not get contacts for search with id: " . $search_id .
                ". Exception msg: " . $e);
            return null;
        }
    }
}
