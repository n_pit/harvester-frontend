<?php namespace App\Services;

use Carbon\Carbon;

class DatesHelper{

    protected $timeZone = "Europe/Athens";

    // get finely formatted date from DB
    public function getDateStringFromDBDate($date){
        if(!empty($date)) {
            return $date->format("d/m/Y");
        } else {
            return "N/A";
        }
    }

    // gets current time string
    public function getCurrentTimeString(){
        return Carbon::now($this->timeZone)->toDateTimeString();
    }
}
