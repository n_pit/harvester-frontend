<?php

namespace App\Services\EmailServices;

use App\Services\SearchDBStorageService;

class MessageCreatorService{
    
    public function getMessageForNewRegisterNotification(){
        $searchDBStorageService = new SearchDBStorageService();
        $search = $searchDBStorageService->getNewlyRegisteredUserFirstSearch();
        if($search != null and \Auth::check()) {
            $keywords = $searchDBStorageService->getSearchKeywordsFormattedForAPICall($search->id);
            $registeredUserEmail = \Auth::user()->email;
            $message = \Lang::get('message_creator_service.registered_email') . $registeredUserEmail;
            if ($keywords != null) {
                $message .= "\n\n" . \Lang::get('message_creator_service.keywords_added') . $keywords;
            } else {
                $message .= "\n\n" . \Lang::get('message_creator_service.no_search_yet');
            }
            return $message;
        } else {
            return null;
        }
    }
}
