<?php

namespace App\Services\EmailServices;

use Illuminate\Support\Facades\Log;

class SendEmailService{

    const EMAIL_TO_SEND_NOTIFICATIONS = "vgia@scify.org";

    public function sendRawEmailToApp($email, $subject, $message){
        try {
            \Mail::raw($message, function ($m) use ($email, $subject) {
                $m->from($email);
                $m->to(env('APP_MAIL_ADDRESS'), env('APP_MAIL_NAME'))->subject($subject);
            });
            return "true";
        } catch(\Exception $e){
            Log::info("Problem with mail: " . $e);
            return "false";
        }
    }

    public function sendRawEmailFromApp($email, $subject, $message){
        try {
            \Mail::raw($message, function ($m) use ($email, $subject) {
                $m->from(env('APP_MAIL_ADDRESS'), env('APP_MAIL_NAME'));
                $m->to($email)->subject($subject);
            });
            return "true";
        } catch(\Exception $e){
            Log::info("Problem with mail: " . $e);
            return "false";
        }
    }

    public function sendNewRegisteredUserEmail(){
        $messageCreatorService = new MessageCreatorService();
        $message = $messageCreatorService->getMessageForNewRegisterNotification();
        if($message != null) {
            return $this->sendRawEmailFromApp(self::EMAIL_TO_SEND_NOTIFICATIONS,
                \Lang::get('message_creator_service.new_registration'), $message);
        }
    }
}
