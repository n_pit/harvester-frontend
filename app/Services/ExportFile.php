<?php

namespace App\Services;

use App\Models\Contact;
use App\Models\Search;
use App\Services\DatesHelper;

class ExportFile{
    
    public function exportContactsCSVFile($searchId){
        $search = Search::find($searchId);
        if($search != null) {
            $csv = $this->createContactsCSVFile($searchId);
            if (!empty($csv)) {
                $datesHelper = new DatesHelper();
                $fileName = $search->name . "_" . $datesHelper->getCurrentTimeString() . ".csv";
                $fileName = str_replace(" ", "_", $fileName);
                return response($csv)
                    ->header('Content-Type', 'application/csv')
                    ->header('Content-Disposition', 'attachment; filename="' . $fileName . '"')
                    ->header('Pragma', 'no-cache')
                    ->header('Expires', '0');
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
    
    private function createContactsCSVFile($searchId){
        $searchContacts = Contact::where("search_id", "=", $searchId)->get();
        if($searchContacts != null) {
            $transformContactsToViewModelArrayService = new TransformContactsToContactsViewModelArray($searchContacts);
            $contactsViewModels = $transformContactsToViewModelArrayService->getArrayOfContactsViewModelsFromContacts();
            if(empty($contactsViewModels)){
                return null;
            }
            $p = "backoffice/display_search.";
            // insert column titles in csv
            $export_data = \Lang::get($p . 'company_name') . "," . \Lang::get($p . "email") . "," .
                \Lang::get($p . "contact_info") . "," . \Lang::get($p . "address") . "," .
                \Lang::get($p . "social_media") . "," . "\n";
            foreach ($contactsViewModels as $contact) {
                $export_data .= "\"" . $contact->getDomain() . "\",\"" .
                    $contact->getEmail() . "\",\"" .
                    $contact->getPhone() . "\",\"" .
                    $contact->getAddress() . "\",\"" .
                    $contact->formatSocialMediaWithoutLinks() . "\"\n";
            }
            return $export_data;
        } else {
            return null;
        }
    }
}
