<?php

namespace App\Services\PermissionsToDisplaySearch;

use App\Models\Search;

class CheckUsersPermissions{
    
    public function searchBelongsToLoggedInUserAndIsCompleted($searchId){
        $search = Search::find($searchId);
        if($this->searchExists($search) and $this->searchIsCompleted($search->search_status)){
            if($search->user_id == \Auth::user()->id){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    private function searchExists($search){
        if($search != null){
            return true;
        } else {
            return false;
        }
    }

    private function searchIsCompleted($search_status){
        if($search_status == 100){
            return true;
        } else {
            return false;
        }
    }

    public function searchBelongsToLoggedInUser($searchId){
        $search = Search::find($searchId);
        if($this->searchExists($search)){
            if($search->user_id == \Auth::user()->id){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
