<?php namespace App\Services;

use App\Models\Contact;
use App\Models\Keyword;
use App\Models\Search;
use Illuminate\Support\Facades\Log;

class SearchDBStorageService{

    // calls functions to store info to searches and keywords DB tables
    public function storeSearchToDB($keywordsArray, $locationSpecified){
        if(\Auth::check()) {
            $user_id = \Auth::user()->id;
        } else {
            $user_id = null;
        }
        $search = $this->storeSearchInfoToDB($user_id, $locationSpecified);
        $keywords = $this->storeSearchKeywordsToDB($keywordsArray, $search->id);
        return array(
            'search_name' => $search->name,
            'search_id' => $search->id,
            'keywords' => $keywords,
        );
    }

    // inserts a new row to searches DB table
    private function storeSearchInfoToDB($user_id, $location_specified){
        $datesHelper = new DatesHelper();
        $search = new Search();
        $search->user_id = $user_id;
        $search->name = "Search " . $datesHelper->getCurrentTimeString();
        $search->location = $location_specified;
        $search->save();
        return $search;
    }

    // calls the functions necessary to store keywords to DB
    private function storeSearchKeywordsToDB($keywordsArray, $search_id){
        $this->storeKeywordsToDB($keywordsArray, $search_id);
        return $keywordsArray;
    }

    // stores keywords to DB
    private function storeKeywordsToDB($keywords, $search_id){
        foreach($keywords as $keyword){
            $temp = new Keyword();
            $temp->keyword = $keyword;
            $temp->search_id = $search_id;
            $temp->save();
        }
    }

    public function storeSearchesStatusesToDB($data){
        foreach ($data as $search_id => $singleSearchData) {
            $search = Search::find($search_id);
            if ($search != null) {
                if ($search->search_status != 100) {
                    $search->search_status = $singleSearchData->progressBar;
                    $search->save();
                }
            }
        }
    }

    public function deleteSearchFromDB($search_id){
        try {
            Contact::where("search_id", "=", $search_id)->delete();
            Keyword::where("search_id", "=", $search_id)->delete();
            Search::find($search_id)->delete();
            return "true";
        } catch(\Exception $e){
            Log::info("Could not delete the search with id " . $search_id . " from the DB.");
            return "false";
        }
    }

    public function getSearchInfoAndContactsFromAPICall($search_id, $contacts){
        $searchInfo = $this->getSearchInfo($search_id);
        $searchInfo['results'] = $contacts;
        return $searchInfo;
    }

    // gets search info by search id
    public function getSearchInfo($search_id){
        $searchFormatted = null;
        $searchInfo = Keyword::join('searches', 'keywords.search_id', '=', 'searches.id')
            ->selectRaw('keywords.id as k_id, searches.id as s_id, name, searches.created_at, search_status,' .
                'got_contacts_found, keyword')
            ->where('searches.id', '=', $search_id)->get();
        $contactsFoundNumber = Contact::where('search_id', '=', $search_id)->count();
        if(count($searchInfo)){
            $searchFormatted = $this->correctlyFormatSearchesInfo($searchInfo[0]->name,
                $searchInfo[0]->s_id,
                $searchInfo[0]->created_at,
                $searchInfo[0]->search_status,
                $searchInfo[0]->got_contacts_found,
                $searchInfo,
                $contactsFoundNumber
            );
        }
        return $searchFormatted;
    }

    // formats the search info to an appropriate for the view form
    private function correctlyFormatSearchesInfo($search_name, $search_id, $search_start_date, $search_status, $got_contacts_found, $searches, $contactsFoundNumber){
        $result = array();
        $result['search_name'] = $search_name;
        $result['search_id'] = $search_id;
        $result['search_start_date'] = $search_start_date;
        $result['search_status'] = $search_status;
        $result['got_contacts_found'] = $got_contacts_found;
        $temp = array();
        foreach($searches as $search){
            $temp[$search->k_id] = $search->keyword;
        }
        $result['contacts_found_number'] = $contactsFoundNumber;
        $result['keywords'] = $temp;
        return $result;
    }
    
    public function getSearchResultsArray($search_id){
        $contacts = Contact::where('search_id', '=', $search_id)->get();
        $transformContactsToViewModelArrayService = new TransformContactsToContactsViewModelArray($contacts);
        return $transformContactsToViewModelArrayService->getArrayOfContactsViewModelsFromContacts();
    }    

    public function storeContactsFoundToDB($data, $search_id){
        $this->contactsAreNowStoredToDB($search_id);
        if(!empty($data)) {
            $keywords = $this->getKeywordsAssociativeArrayFromSearchId($search_id);
            foreach ($data as $contactFromJSON) {
                $contactForDBInsertion = new Contact();
                $contactForDBInsertion->domain = $contactFromJSON->domain;
                $contactForDBInsertion->email = $contactFromJSON->email;
                $contactForDBInsertion->search_id = $search_id;
                $contactForDBInsertion->phone = $contactFromJSON->phone;
                $contactForDBInsertion->address = $contactFromJSON->address;
                $contactForDBInsertion->facebookURL = $contactFromJSON->facebookURL;
                $contactForDBInsertion->twitterURL = $contactFromJSON->twitterURL;
                $contactForDBInsertion->contactId = $contactFromJSON->contactId;
                if($keywords != null) {
                    $contactForDBInsertion->relatedQueries = $this->getIdStringForRelatedQueriesKeywords(
                        $contactFromJSON->relatedQueries, $keywords
                    );
                }
                $contactForDBInsertion->save();
                $contactFromJSON->id = $contactForDBInsertion->id;
            }
            $transformContactsToViewModelArrayService = new TransformContactsToContactsViewModelArray($data);
            return $transformContactsToViewModelArrayService->getArrayOfContactsViewModelsFromContacts();
        } else {
            return null;
        }
    }

    private function contactsAreNowStoredToDB($search_id){
        $search = Search::find($search_id);
        $search->got_contacts_found = 1;
        $search->save();
    }

    private function getKeywordsAssociativeArrayFromSearchId($search_id){
        $keywords = Keyword::where('search_id', '=', $search_id)->get();
        if($keywords != null){
            $temp = array();
            foreach ($keywords as $keyword){
                $temp[$keyword->id] = $keyword->keyword;
            }
            return $temp;
        }
        return null;
    }

    private function getIdStringForRelatedQueriesKeywords($relatedQueries, $keywords){
        $temp = "";
        $queriesArray  = explode(" ", $relatedQueries);
        foreach ($queriesArray as $relatedQuery){
            $key = array_search($relatedQuery, $keywords);
            if($key != null){
                $temp .= $key . " ";
            }
        }
        return trim($temp);
    }

    public function getSearchKeywordsFormattedForAPICall($search_id){
        $search = Search::find($search_id);
        if($search != null) {
            if (\Auth::check() and $this->searchHasNoOwner($search->user_id)) {
                $search->user_id = \Auth::user()->id;
                $search->save();
            }
            $keywords = Keyword::where('search_id', '=', $search_id)->get()->pluck('keyword')->toArray();
            if($keywords != null) {
                return implode(', ', $keywords);
            }
        }
        return null;
    }

    private function searchHasNoOwner($user_id){
        if($user_id == null){
            return true;
        } else {
            return false;
        }
    }

    public function getSearchLocationSpecifiedForAPICall($search_id){
        $search = Search::find($search_id);
        if($search != null){
            return $search->location;
        } else {
            return null;
        }
    }

    public function getNewlyRegisteredUserFirstSearch(){
        if(\Auth::check()){
            return Search::where('user_id', '=', \Auth::user()->id)->first();
        } else {
            return null;
        }
    }

    public function removeContact($contact_id){
        if($contact_id != null) {
            try {
                Contact::find($contact_id)->delete();
                return "true";
            } catch (\Exception $e) {
                Log::info("Couldn't delete the contact. Error message: " . $e);
                return "false";
            }
        } else {
            return null;
        }
    }

    public function getSearchIdOfContact($contact_id){
        $contact = Contact::find($contact_id);
        if($contact != null){
            return $contact->search->id;
        } else {
            return null;
        }
    }
}
