<?php

namespace App\Services;

use App\Models\Contact;
use App\Models\Keyword;
use App\Models\Search;
use Illuminate\Support\Facades\Log;

class SearchDisplayService{

    private $searchDBStorageService;

    public function __construct(){
        $this->searchDBStorageService = new SearchDBStorageService();
    }

    // gets all user's searches
    public function getAllUserSearchesInfo(){
        $userId = \Auth::user()->id;
        $searches = Search::where('user_id', '=', $userId)->orderBy('id', 'DESC')->get();
        $allSearchesInfo = array();
        foreach($searches as $search){
            $searchInfo = $this->searchDBStorageService->getSearchInfo($search->id);
            if($searchInfo != null){
                array_push($allSearchesInfo, $searchInfo);
            }
        }
        return $allSearchesInfo;
    }

    public function getSearchInfoAndSearchResults($id){
        $searchInfo = $this->searchDBStorageService->getSearchInfo($id);
        $searchResults = $this->searchDBStorageService->getSearchResultsArray($id);
        $searchInfo['results'] = $searchResults;
        return $searchInfo;
    }    

    public function editSavedSearchName($request){
        $current_search_id = $request['id'];
        // find the relative search by the id
        $current_search = Search::find($current_search_id);
        // update the name of the specific search row
        if($current_search != null) {
            $current_search->name = $request['new_value'];
            $current_search->save();
        }
    }
}
