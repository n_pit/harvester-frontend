<?php

namespace App\Services;

use App\Services\PermissionsToDisplaySearch\CheckUsersPermissions;
use Illuminate\Support\Facades\Log;

class SearchPerformingService{

    private $communicateWithREST;
    private $searchDBStorageService;

    public function __construct(){
        $this->communicateWithREST = new CommunicateWithREST();
        $this->searchDBStorageService = new SearchDBStorageService();
    }

    public function performSearchFromKeywordsAndStoreIt($keywords, $location_specified){
        try {
            $search = $this->searchDBStorageService->storeSearchToDB($keywords, $location_specified);
            $response = $this->performSearchFromKeywords($search, $location_specified);
            if($response == null){
                $this->searchDBStorageService->deleteSearchFromDB($search['search_id']);
            }
            return $response;
        } catch(\Exception $e) {
            Log::info("Could not perform search. Error: " . $e);
            return null;
        }
    }

    private function performSearchFromKeywords($search, $location_specified){
        $search_id = $search['search_id'];
        $keywords = implode(', ', $search['keywords']);
        // post the above keywords (separated with comma and no spaces) to api for processing
        return $this->communicateWithREST->apiStartContactHarvestingFromKeywords($search_id, $keywords,
            $location_specified);
    }

    public function performSearchAfterAuthentication($search_id){
        $keywords = $this->searchDBStorageService->getSearchKeywordsFormattedForAPICall($search_id);
        $location_specified = $this->searchDBStorageService->getSearchLocationSpecifiedForAPICall($search_id);
        return $this->communicateWithREST->apiStartContactHarvestingFromKeywords($search_id,
            $keywords, $location_specified);
    }

    public function getSearchesStatusesForUserAndStoreThem(){
        $response = $this->communicateWithREST->apiSearchStatusOfUser();
        if ($response != null and $response->status == "success") {
            $data = $response->data;
            $this->searchDBStorageService->storeSearchesStatusesToDB($data);
            return json_encode($data);
        } else {
            return null;
        }
    }

    public function fetchContactsFromApiStoreThemAndRetrieveThem($search_id){
        $response = $this->communicateWithREST->apiGetContactsFromKeywordsSearch($search_id);
        if($response != null and $response->status == "success") {
            // stores contacts
            $contacts = $this->searchDBStorageService->storeContactsFoundToDB($response->data, $search_id);
            if($contacts != null){
                return $this->searchDBStorageService->getSearchInfoAndContactsFromAPICall($search_id, $contacts);
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public function deleteSearch($searchId){
        $checkUsersPermissions = new CheckUsersPermissions();
        if($checkUsersPermissions->searchBelongsToLoggedInUser($searchId)) {
            // delete everything stored in DB that has to do with this search
            return $this->searchDBStorageService->deleteSearchFromDB($searchId);
        } else {
            Log::info("Search does not belong to logged in user");
            return "false";
        }
    }
    
    public function cancelSearch($searchId){
        $checkUsersPermissions = new CheckUsersPermissions();
        if($checkUsersPermissions->searchBelongsToLoggedInUser($searchId)) {
            $response = $this->communicateWithREST->apiCancelContactHarvestingFromKeywords($searchId);
            Log::info("Search delete response: " . json_encode($response));
            if ($response != null and $response->status == "success") {
                return $this->deleteSearch($searchId);
            } else {
                return "false";
            }
        } else {
            return "false";
        }
    }
    
    public function removeContactFromDB($contactId){
        $checkUsersPermissions = new CheckUsersPermissions();
        if($checkUsersPermissions->searchBelongsToLoggedInUser(
            $this->searchDBStorageService->getSearchIdOfContact($contactId))) {
            return $this->searchDBStorageService->removeContact($contactId);
        } else {
            return "false";
        }
    }
}
