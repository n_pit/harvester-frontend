<?php

namespace App\Services;

use App\Models\ViewModels\ContactsViewModel;

class TransformContactsToContactsViewModelArray{

    private $contacts = null;

    public function __construct($contacts){
        $this->contacts = $contacts;
    }

    public function getArrayOfContactsViewModelsFromContacts(){
        $results = array();
        if(!empty($this->contacts)) {
            foreach ($this->contacts as $contact) {
                $contactViewModel = new ContactsViewModel(
                    $contact->id,
                    $contact->domain,
                    $contact->email,
                    $contact->phone,
                    $contact->address,
                    $contact->facebookURL,
                    $contact->twitterURL,
                    $contact->contactId,
                    $contact->relatedQueries
                );
                array_push($results, $contactViewModel);
            }
            return $results;
        } else {
            return null;
        }
    }
}
