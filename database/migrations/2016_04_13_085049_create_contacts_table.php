<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('domain', 100)->nullable();
            $table->string('email', 50)->nullable();
            $table->string('phone')->nullable();
            $table->string('address')->nullable();
            $table->string('facebookURL')->nullable();
            $table->string('twitterURL')->nullable();
            $table->integer('search_id')->unsigned();
            $table->foreign('search_id')->references('id')->on('searches');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contacts');
    }
}
