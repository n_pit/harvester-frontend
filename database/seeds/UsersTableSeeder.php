<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create(array(
            'name' => 'Yolo Swaggins',
            'email' => 'test@test.com',
            'password' => Hash::make('test62!'),
        ));

        User::create(array(
            'name' => 'Christos Dimizas',
            'email'=> 'x_dimizas@hotmail.com',
            'password' => Hash::make('1234qwer'),
        ));
    }
}
