function RunningTasksExist(){
    return ($(".running-box").length > 0);
}

// function StoreContactsAndDisplayContactsFoundCount($search_id){
//     $.ajax({
//         url: $("#search_list").data("store-contacts-url") + "/" + $search_id,
//         method: 'get',
//         async: false,
//         success: function($response){
//             // set the number of contacts found
//             var $search = $("#search_" + $search_id);

//             if($response != "") {
//                 $search.find(".search-results-counter").html($response);
//                 $search.find(".running").addClass("hide");
//                 $search.find(".completed").removeClass("hide");
//             } else {
//                 DisplayErrorAlertMessage($("#search_list").data("contacts-problem-msg"));
//             }
//         },
//         error: function(){
//             DisplayErrorAlertMessage($("#search_list").data("server-error-msg"));
//         }
//     });
// }

function CheckProgressOnRunningTasks(){
    var ajaxRequestPending = false;
    var $displayedErrorOnce = false;
    var timer = window.setInterval(function(){
        if (RunningTasksExist()){
            if (!ajaxRequestPending) {
                $.ajax({
                    url: $("#search_list").data("searches-status-url"),
                    method: 'get',
                    beforeSend: function () {
                        ajaxRequestPending = true;
                    },
                    success: function (response) {
                        if(response != "") {
                            // iterate to the response and map status to the task
                            var $json = JSON.parse(response);
                            var $keys = Object.keys($json);
                            for (var i = 0; i < $keys.length; i++) {
                                var $progressBarValue = $json[$keys[i]]['progressBar'];
                                var $search = $("#search_" + $keys[i]);
                                if ($search.find(".result-box").hasClass("running-box")) {
                                    $search.find(".progress-bar").attr("aria-valuenow", $progressBarValue).css({'width': $progressBarValue + '%'});
                                    if ($progressBarValue == 100) {
                                        $search.find(".searchAnchor").removeClass("inactive-link").addClass("pointer");
                                        $search.find(".result-box").removeClass("running-box").addClass("completed-box").addClass("pointer");
                                        $search.find(".click-to-cancel").addClass("hide");
                                        $search.find(".click-to-edit-value").removeClass("hide");
                                        $search.find(".counter-info").html($("#search_list").data("completed"));
                                    }
                                }
                            }
                        } else {
                            if(!$displayedErrorOnce) {
                                DisplayErrorAlertMessage($("#search_list").data("server-error-msg"));
                                $displayedErrorOnce = true;
                            }
                        }
                    },
                    complete: function(){
                        ajaxRequestPending = false;
                    },
                    error: function(){
                        if(!$displayedErrorOnce) {
                            DisplayErrorAlertMessage($("#search_list").data("server-error-msg"));
                            $displayedErrorOnce = true;
                        }
                    }
                });
            }
        } else {
            window.clearInterval(timer);
        }
    }, 10*1000);
}

function DisplayCancelConfirmationModalAndSetCancelUrl($this){
    $("#cancel-modal").modal("show");
    $cancelUrl = $this.parents(".search-box").data("cancel-search-url");
}

function MakeAjaxCallToCancelSearch(){
    $.ajax({
        url: $cancelUrl,
        method: "GET",
        success: function($response){
            if($response == "true"){
                // refresh page
                location.reload();
            } else {
                DisplayErrorAlertMessage($("#search_list").data("server-error-msg"));
                return false;
            }
        },
        error: function(){
            DisplayErrorAlertMessage($("#search_list").data("server-error-msg"));
            return false;
        }
    });
}

function init(){
    CheckProgressOnRunningTasks();

    $('#all_searches').addClass('selected-menu-item');
    $("#search_list").on('delegate', "*", "focus blur", function() {
        var input_elem = $( this );
        setTimeout(function() {
            input_elem.toggleClass( "edit-border", input_elem.is( ":focus" ) );
        }, 0 );
    });

    $('body').on('click', '.completed-box', function(){
        window.location = $(this).siblings('.border-right-gray').find('.current-value').data('url');
    });

    $('.searchAnchor').on('click', function(){
        // if link is inactive, do nothing
        if($(this).hasClass("inactive-link")){
            return false;
        } else {
            window.location = $(this).data('url');
        }
    });

    $('body').on('click', '.click-to-cancel', function(){
        DisplayCancelConfirmationModalAndSetCancelUrl($(this));
    });

    $('.cancel-modal-btn').on('click', MakeAjaxCallToCancelSearch);
}

$(document).ready(init);

var $cancelUrl;
