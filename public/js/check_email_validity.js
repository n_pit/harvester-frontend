function CheckEmailValidityDisplayResultAndHandleTheSendButton(){
    var emailRegEx = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
    if($("input[name='contact_email']").val().search(emailRegEx) == -1){
        $(".color-red").removeClass("hide");
        $(".color-green").addClass("hide");
        $(".email-modal-btn").attr("disabled", "disabled");
    } else {
        $(".color-green").removeClass("hide");
        $(".color-red").addClass("hide");
        $(".email-modal-btn").removeAttr("disabled");
    }
}

function init(){
    $("input[name='contact_email']").on("blur", CheckEmailValidityDisplayResultAndHandleTheSendButton);
}

$(document).ready(init);
