function RedirectToDashboard($closeSingleSearchButton){
    window.location.replace($closeSingleSearchButton.data("redirect-url"));
    return true;
}

function DisplayDeleteConfirmationModal(){
    $("#delete-modal").modal("show");
}

function DisplaySendEmailModal(){
    jQuery.noConflict();
    $("#email-modal").modal("show");
}

function DisplayExportModal(){
    $("#export-modal").modal("show");
}

function DisplayIntegrateModal(){
    jQuery.noConflict();
    $("#integrate-modal").modal("show");
}

function SetContactToRemoveUrl(url){
    contact_to_remove_url = url;
}

function SetRowToRemove(row){
    row_to_remove = row;
}

function AjaxToRemoveContact(){
    $.ajax({
        url: contact_to_remove_url,
        method: "GET",
        success: function($response){
            if($response == "true"){
                $("#results-table").DataTable().row(row_to_remove).remove().draw(false);
                console.log("Contact deleted.");
                $("#remove-contact-modal").modal("hide");
                ReduceContactsFoundCounter();
            } else {
                DisplayErrorAlertMessage($(".delete-btn").data("msg-error-occurred"));
            }
        },
        error: function(){
            DisplayErrorAlertMessage($(".delete-btn").data("msg-error-occurred"));
        }
    });
}

function ReduceContactsFoundCounter(){
    $(".search-results-counter").html(parseInt($(".search-results-counter").html() - 1));
}

function MakeAjaxCallToDeleteSearch(){
    var $error_occurred = $(".delete-btn").data("msg-error-occurred");
    $.ajax({
        url: $(".search-box").data("delete-url"),
        method: "GET",
        success: function($response){
            if($response == "true"){
                RedirectToDashboard($(".close-single-search"));
            } else {
                DisplayErrorAlertMessage($error_occurred);
                return false;
            }
        },
        error: function(){
            DisplayErrorAlertMessage($error_occurred);
            return false;
        }
    });
}

function HideDeleteButton(){
    $(".delete-btn").hide();
}

function ShowDeleteButton(){
    $(".delete-btn").show();
}

function DisplayAppropriateTextForSelectedKeywords(){
    var count = selected_keywords.length;
    var selectedKeywordsCount = $(".selected-keywords-count");
    var text = "(" + selectedKeywordsCount.data("selected-keywords-text");
    switch(count){
        case 0:
            selectedKeywordsCount.html("1").css("opacity", 0);
            break;
        case 1:
            text += count + selectedKeywordsCount.data("single-keyword") + " " +
                "<a href='javascript:void(0)' class='remove-all'>" + selectedKeywordsCount.data("remove-all") +
                "</a>)";
            selectedKeywordsCount.html(text).css("opacity", 1);
            break;
        default:
            text += count + selectedKeywordsCount.data("multiple-keywords") + " " +
                "<a href='javascript:void(0)' class='remove-all'>" + selectedKeywordsCount.data("remove-all") +
                "</a>)";
            selectedKeywordsCount.html(text).css("opacity", 1);
    }
}

function SelectKeywordsAndFilterDataTable(){
    var $this = $(this);
    var index;
    if($this.hasClass("selected-keyword")){
        index = selected_keywords.indexOf($this.data("keyword-id"));
        // if element is found in array
        if(index != -1){
            $this.removeClass("selected-keyword");
            selected_keywords.splice(index, 1);
        }
    } else {
        $this.addClass("selected-keyword");
        selected_keywords.push($this.data("keyword-id"));
    }
    $("#results-table").DataTable().draw();
    DisplayAppropriateTextForSelectedKeywords();
}

function RemoveAllSelectedKeywords(){
    selected_keywords = [];
    $(".keyword").removeClass("selected-keyword");
    $("#results-table").DataTable().draw();
    DisplayAppropriateTextForSelectedKeywords();
}

function init(){
    $("#results-table").DataTable();

    $(".close-single-search").on("click", function(){
        return RedirectToDashboard($(this));
    });

    $(".click-to-edit-value").on("click", HideDeleteButton);
    $(".done-btn").on("click", ShowDeleteButton);
    $(".cancel-btn").on("click", ShowDeleteButton);

    $(".delete-btn").on("click", DisplayDeleteConfirmationModal);
    $(".delete-modal-btn").on("click", MakeAjaxCallToDeleteSearch);

    $(".email a").on("click", DisplaySendEmailModal);

    $(".export a").on("click", DisplayExportModal);

    $(".integrate a").on("click", DisplayIntegrateModal);

    $("body").on("click", ".remove-contact-btn", function(){
        SetContactToRemoveUrl($(this).data("remove-contact-url"));
        SetRowToRemove($(this).parents("tr"));
    });

    $(".remove-contact-modal-btn").on("click", AjaxToRemoveContact);

    $(".keyword").on("click", SelectKeywordsAndFilterDataTable);

    $("body").on("click", ".remove-all", RemoveAllSelectedKeywords);
}

function initializeDataTablesCustomFilter(){
    $.fn.dataTable.ext.search.push(
        function(settings, data, dataIndex) {
            // display all, if no keyword is selected to filter the table
            if(selected_keywords.length === 0){
                return true;
            }
            var temp  = data[12].split(" ");
            var keywordIndex;
            for(var i = 0; i < temp.length; i++){
                keywordIndex = selected_keywords.indexOf(parseInt(temp[i]));
                if (keywordIndex != -1){
                    return true;
                }
            }
            return false;
        }
    );
}

function initializeIntroJSIfNeeded(){
    if(typeof(Storage) !== "undefined") {
        var accustomedVisitor = localStorage.getItem("accustomed_visitor");
        if(accustomedVisitor !== "true"){
            localStorage.setItem("accustomed_visitor", "true");
            introJs().start();
        }
    } else {
        introJs().start();
    }
}

$(document).ready(function(){
    init();
    initializeDataTablesCustomFilter();
    initializeIntroJSIfNeeded();
});

var contact_to_remove_url;
var row_to_remove;
var selected_keywords = [];
