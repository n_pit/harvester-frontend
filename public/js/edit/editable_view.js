function MakeAjaxCallForSearchRename($url, $json, $this){
    $.ajax({
        url: $url,
        data: $json,
        method: 'post',
        success: function(){
            $this.parent().siblings('.current-value').html($json['new_value']);
        }
    });
}

function ShowInputToEditValue(){
    $(this).siblings('.edit-value').show();
    var $textInput = $(this).siblings('.edit-value').find('input.new-value');
    $textInput.toggleClass('edit-border show-input').focus();
    var tmpStr = $textInput.val();
    $textInput.val('');
    $textInput.val(tmpStr);
    $(this).siblings('.current-value').toggle();
    $(this).siblings('.enumerate-search').hide(); // TODO: figure out how to remove it from here
    $(this).hide();
}

function SaveTheNewValue(){
    var $new_value = $(this).siblings('input[name="new_value"]').val();
    var $id = $(this).parent('.edit-value').data("edit-id");
    var $token = $(this).siblings('input[name="_token"]').val();
    var $url = $(this).parent('.edit-value').data("edit-url");
    var $json = {'_token': $token, 'id': $id, 'new_value': $new_value};
    MakeAjaxCallForSearchRename($url, $json, $(this));
    $(this).siblings('input.new-value').toggleClass('edit-border show-input');
    $(this).parent().siblings('.current-value').toggle();
    $(this).parent().hide();
    $(this).parent().siblings('.click-to-edit-value').show();
    $(this).parent().siblings('.enumerate-search').show(); // TODO: figure out how to remove it from here
    return false;
}

function HideEditValueInput(){
    $(this).parent().siblings('.current-value').toggle();
    $(this).parent().parent().find('input.new-value').toggleClass('edit-border show-input');
    $(this).parent().hide();
    $(this).parent().siblings('.click-to-edit-value').show();
    $(this).parent().siblings('.enumerate-search').show(); // TODO: figure out how to remove it from here
}

function init(){
    $('.click-to-edit-value').on('click', ShowInputToEditValue);

    $('.done-btn').on('click', SaveTheNewValue);

    $('.cancel-btn').on('click', HideEditValueInput);
}

$(document).ready(init);
