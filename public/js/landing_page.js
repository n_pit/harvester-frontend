function MakeAjaxToSendEmail(){
    var email = $("input[name='contact_email']").val();
    var subject = $("input[name='subject']").val();
    var message = $("textarea[name='message']").val();
    var data = {'email': email, 'subject': subject, 'message': message};
    $.ajax({
        url: $("#contact-modal").data("contact-us-url"),
        method: 'get',
        data: data,
        beforeSend: function(){
            $("#contact-modal").modal("hide");
        },
        success: function(response){
            if(response == "true"){
                DisplaySuccessAlertMessage($("#contact-modal").data("contact-success-msg"));
            } else {
                DisplayErrorAlertMessage($("#contact-modal").data("contact-error-msg"));
            }
        },
        error: function(){
            DisplayErrorAlertMessage($("#contact-modal").data("contact-error-msg"));
        }
    });
}

function init(){
    // initialize select2 plugin
    $(".js-select-search-tags").select2({
        tags: true,
        tokenSeparators: [','],
        placeholder: $(this).data("placeholder")
    });

    $(".email-modal-btn").on("click", MakeAjaxToSendEmail);

    // bootstrap popover
    $("#searchLead").siblings().find("input").popover({
        placement: "left",
        trigger: "focus",
        content: $("#searchLead").data("popover-content")
    });
}

$(document).ready(init);
