$(document).ready(function(){

    $("#show-menu").on("click", function(){
        animateSideBarContentOnClick();
    });

    $(window).resize(function(){
        animateSideBarContentOnWindowResize();
    });
});

function getSideBarWidth(){
    var $width = $(window).width();
    if($width > 700){
        return 250;
    } else if($width > 600){
        return 160;
    } else {
        return 90;
    }
}

function animateSideBarContentOnClick(){
    var $width = getSideBarWidth();
    if($body.hasClass("menu-open")) {
        $("#side-bar").animate({left: $width * -1}, 1000);
        $("#content").animate({left: 0}, 1000);
        $body.removeClass("menu-open");
    } else {
        $("#side-bar").animate({left: 0}, 1000);
        $("#content").animate({left: $width}, 1000);
        $body.addClass("menu-open");
    }
}

function animateSideBarContentOnWindowResize(){
    var $width = getSideBarWidth();
    if($width != $("#sidebar").width()) {
        if ($body.hasClass("menu-open")) {
            $("#side-bar").animate({width: $width}, 0);
            $("#content").animate({left: $width}, 0);
        } else {
            $("#side-bar").animate({width: $width}, 0);
            $("#content").animate({left: 0}, 0);
        }
    }
}

var $body = $("body");
