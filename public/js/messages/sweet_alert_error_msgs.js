function DisplayErrorAlertMessage($msg) {
    swal({
        title: "",
        text: $msg,
        type: "error",
        confirmButtonText: "OK",
        confirmButtonColor: "#274289"
    });
}
