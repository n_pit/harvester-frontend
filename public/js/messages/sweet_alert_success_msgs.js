function DisplaySuccessAlertMessage($msg) {
    swal({
        title: "",
        text: $msg,
        type: "success",
        confirmButtonText: "OK",
        confirmButtonColor: "#274289"
    });
}
