function init() {
    $('#new_search').addClass('selected-menu-item');

    // Select all tabs
    $('.nav-tabs a').on('click', function () {
        $(this).tab('show');
        $(this).closest('li.active').find('a').addClass('darkColor');
        $(this).closest('li').not('.active').removeClass('darkColor');
    });

    // Initialize select2
    $(".js-select-search-keywords").select2({
        tags: true,
        tokenSeparators: [','],
        placeholder: $(this).data("placeholder")
    });
    // $(".js-select-search-urls").select2({
    //     tags: true,
    //     theme: "classic",
    //     placeholder: "Type in URLs"
    // });

    // bootstrap popover
    $(".js-select-search-keywords").siblings().find("input").popover({
        placement: "left",
        trigger: "focus",
        content: $("#keywords").data("popover-content")
    });

    // DO NOT submit on enter key
    $('.search-from-keywords').keypress(function (e) {
        if (e.which == 13) return false;
    });

    $('.search-from-urls').keypress(function (e) {
        if (e.which == 13) return false;
    });
}

$(document).ready(init);
