function checkNumberOfKeywordsValidity($keywordsCounter){
    if($keywordsCounter < 3) {
        DisplayErrorAlertMessage($(".js-select-search-tags").data("error-msg"));
        return false;
    } else {
        return true;
    }
}

function init(){
    $(".select2-form").on("submit", function(){
        return checkNumberOfKeywordsValidity($(this).find("select:first option").length);
    });
}

$(document).ready(init);
