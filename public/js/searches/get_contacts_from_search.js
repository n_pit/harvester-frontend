function StoreContactsAndDisplayThem(){
    $.ajax({
        url: $(".search-box").data("store-contacts-url"),
        method: 'get',
        async: false,
        success: function($response){
            if($response != "") {
                $(".search-results-counter").html($response.length);
                $("#results-table").DataTable().destroy();
                var $tbody = $("#results-table tbody");
                var $response_size = $response.length;
                for(var i = 0; i < $response_size; i++) {
                    var $social_media = null;
                    if($response[i].fb_url != null) {
                        $social_media = "<a href='" + $response[i].fb_url + "' target='_blank'>" +
                            $response[i].fb_url + "</a>"
                    }
                    if($response[i].fb_url != null && $response[i].tw_url != null){
                        $social_media += ", ";
                    }
                    if($response[i].tw_url != null){
                        if($social_media == null){
                            $social_media = "<a href='" + $response[i].tw_url + "' target='_blank'>" +
                                $response[i].tw_url + "</a>";
                        } else {
                            $social_media += "<a href='" + $response[i].tw_url + "' target='_blank'>" +
                                $response[i].tw_url + "</a>";
                        }
                    }

                    var $tableRow;
                    var $should_put_bottom_border = i - $response_size - 1;
                    if($should_put_bottom_border > 0){
                        $tableRow = "<tr class=\"bottom-border\">";
                    } else {
                        $tableRow = "<tr>";
                    }
                    $tableRow += "<td class=\"border-right border-top\">" + (i + 1) + "</td>";
                    $tableRow += "<td class=\"border-right border-top\">";
                    if($response[i].name != null){
                        $tableRow += $response[i].name;
                    }
                    $tableRow += "</td>";
                    $tableRow += "<td class=\"border-right border-top\">";
                    if($response[i].email != null) {
                        $tableRow += "<a href='mailto:" + $response[i].email +
                        "'>" + $response[i].email + "</a>";
                    }
                    $tableRow += "</td>";
                    $tableRow += "<td class=\"border-right border-top\">";
                    if($response[i].phone != null){
                        $tableRow += $response[i].phone;
                    }
                    $tableRow += "</td>";
                    $tableRow += "<td class=\"border-right border-top\">";
                    if($response[i].address != null){
                        $tableRow += $response[i].address;
                    }
                    $tableRow += "</td>";
                    $tableRow += "<td class=\"border-top\">";
                    if($social_media != null){
                        $tableRow += $social_media;
                    }
                    $tableRow += "</td>";
                    $tableRow += "<td class=\"border-right border-top hide\"></td>";
                    $tableRow += "<td class=\"border-right border-top hide\"></td>";
                    $tableRow += "<td class=\"border-right border-top hide\"></td>";
                    $tableRow += "<td class=\"border-right border-top hide\"></td>";
                    $tableRow += "<td class=\"border-top hide\"></td>";
                    $tableRow += "</tr>";
                    $tbody.append($tableRow);


                    // $("#results-table").DataTable().row.add([
                    //     i + 1,
                    //     $response[i].name,
                    //     $email,
                    //     $response[i].phone,
                    //     $response[i].address,
                    //     $social_media
                    // ]).draw(true);
                }
                $("#results-table").DataTable();
            } else {
                DisplayErrorAlertMessage($(".search-box").data("contacts-problem-msg"));
            }
        },
        error: function(){
            DisplayErrorAlertMessage($(".search-box").data("server-error-msg"));
        }
    });
}

(function(){
    // $.fn.dataTableExt.sErrMode = 'throw';
    StoreContactsAndDisplayThem();
}());
