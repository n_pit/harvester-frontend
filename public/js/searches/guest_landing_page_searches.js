function AjaxToDisplayLoginModalOrInitSearch($form){
    var $keywords = {};
    $("#searchLead :selected").each(function(i, selected){
        $keywords[i] = $(selected).text();
    });
    var $location_search = $(".location-search:checked").val();
    var $data = {keyword:$keywords, '_token': $form.find("input[name='_token']").val(), location_search: $location_search};
    if($location_search == "1"){
        $data['country'] = $("select[name='country']").val();
    }
    $.ajax({
        url: $(".select2-form").attr("action"),
        data: $data,
        method: "post",
        success: function($response){
            if($response != "") {
                $("#login-modal").modal("show");
            }
        }
    });
}

function init(){
    $(".select2-form").submit(function(){
        AjaxToDisplayLoginModalOrInitSearch($(this));
        return false;
    });
}

$(document).ready(init);
