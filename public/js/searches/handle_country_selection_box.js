function DisableSelectBox(){
    $("select[name='country']").attr("disabled", "disabled").css("opacity", 0);
}

function EnableSelectBox(){
    $("select[name='country']").removeAttr("disabled").css("opacity", 1);
}

function init(){
    if($("#search-the-world:checked").is(':checked')){
        $("select[name='country']").attr("disabled", "disabled").css("opacity", 0);
    }

    $("#search-the-world").on("ifChecked", DisableSelectBox);

    $("#search-country").on("ifChecked", EnableSelectBox);
}

$(document).ready(init);
