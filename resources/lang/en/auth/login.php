<?php

    return array(
        'login' => 'Login',
        'email' => 'E-Mail Address',
        'password' => 'Password',
        'remember' => 'Remember Me',
        'forgot_password' => 'Forgot Your Password?',
    );
