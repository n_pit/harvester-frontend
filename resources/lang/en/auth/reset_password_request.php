<?php

    return array(
        'reset_password' => 'Reset Password',
        'email' => 'E-Mail Address',
        'send_reset_link' => 'Send Password Reset Link',
    );
