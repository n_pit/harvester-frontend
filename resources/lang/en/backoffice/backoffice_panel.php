<?php

    return array(
        'menu' => 'MENU',
        'new' => 'New search',
        'all' => 'My searches',
        'display_search' => 'Display Search',
        'terms' => 'Terms & Conditions',
        'privacy' => 'Privacy Policy',
    );
