<?php

    return array(
        'warning_text' => 'Your search has been triggered. You will be notified as soon as we have your leads ready!',
        'error_text' => 'Your search could not be triggered. Please try again later.',
        'contacts_found' => ' contacts found',
        'rename' => 'RENAME',
        'done' => 'DONE',
        'cancel' => 'CANCEL',
        'results' => 'SEE RESULTS',
        'running' => 'running...',
        'compiled-list' => 'Compiled Keywords list: ',
        'created_on' => 'created on ',
        'success' => ' The progress finished successfully !',
        'server_problem' => 'A server error occurred.',
        'get_contacts_problem' => 'Contacts could not be retrieved for this search.',
        'completed' => 'completed',
        'cancel_modal_title' => 'CANCEL CONFIRMATION',
        'cancel_modal_text' => 'Are you sure you want to cancel this search?<br/>This will delete the search from the list as well.',
        'close' => 'Close',
        'ok' => 'OK',
    );
