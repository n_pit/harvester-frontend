<?php

    return array(
        'keywords_title'    => 'Describe your leads.',
        'keywords_text' => 'Add the keywords that describe them best.',
        'keywords_placeholder' => 'ex.: buyers, wholesale, chocolate',
        'new-search'       => 'New Search',
        'keywords'         => 'KEYWORDS',
        'keywords_popover_content' => 'Make sure your keywords are separated with commas \',\'',
        'urls'             => 'URLs',
        'urls-text-1'      => 'Not sure what keywords to use?',
        'urls-text-2'      => 'Add websites urls of your current or potential client.<br/>We will do the rest.',
        'find_leads'       => 'Find my leads',
    );
