<?php

    return array(
        'new_registration' => 'Contact Harvester | New Registration',
        'registered_email' => 'A user just registered using this email account: ',
        'keywords_added' => 'The search triggered, contained these keywords: ',
        'no_search_yet' => 'The user hasn\'t performed a search, yet.',
    );
