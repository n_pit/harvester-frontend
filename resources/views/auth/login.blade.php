<?php
    $p = "auth/login.";
?>

@extends('layouts.front-layout')

@section('head')
    <title>@lang($p."login")</title>
    <link href="{{ asset("css/landing_page.css") }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/auth/login_register.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('content')
<div class="container width-100-percent height-100-percent margin-top-minus-50">
    <div class="row height-100-percent margin-top-minus-50 margin-bottom-50 margin-left-minus-30 margin-right-minus-30" style="background: url('{{asset('imgs/town.jpeg')}}') fixed no-repeat; background-size: cover;">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default margin-top-30">
                <div class="panel-heading text-center"><p>@lang($p."login")</p></div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                        {!! csrf_field() !!}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">@lang($p."email")</label>

                            <div class="col-md-6">
                                <input type="email" class="form-control" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">@lang($p."password")</label>

                            <div class="col-md-6">
                                <input type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember"> @lang($p."remember")
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary width-100-percent">
                                    <i class="fa fa-btn fa-sign-in"></i>@lang($p."login")
                                </button>

                                <a class="btn btn-link" href="{{ url('/password/reset') }}">@lang($p."forgot_password")</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
