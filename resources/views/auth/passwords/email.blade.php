<?php
    $p = "auth/reset_password_request.";
?>

@extends('layouts.front-layout')

@section('head')
    <title>@lang($p."reset_password")</title>
    <link href="{{ asset("css/landing_page.css") }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/auth/login_register.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('content')
<div class="container width-100-percent height-100-percent margin-top-minus-50">
    <div class="row height-100-percent margin-top-minus-50 margin-bottom-50 margin-left-minus-30 margin-right-minus-30" style="background: url('{{asset('imgs/town.jpeg')}}') fixed no-repeat; background-size: cover;">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default margin-top-30">
                <div class="panel-heading text-center">@lang($p."reset_password")</div>
                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
                        {!! csrf_field() !!}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">@lang($p."email")</label>

                            <div class="col-md-6">
                                <input type="email" class="form-control" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary width-100-percent">
                                    @lang($p."send_reset_link")
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
