<?php
    $p = "auth/register.";
?>

@extends('layouts.front-layout')

@section('head')
    <title>@lang($p."register")</title>
    <link href="{{ asset("css/landing_page.css") }}" rel="stylesheet" type="text/css">
    <link href="{{ asset("css/auth/login_register.css") }}" rel="stylesheet" type="text/css">
@stop

@section('content')
<div class="container width-100-percent height-100-percent margin-top-minus-50">
    <div class="row height-100-percent margin-top-minus-50 margin-bottom-50 margin-left-minus-30 margin-right-minus-30" style="background: url('{{asset('imgs/town.jpeg')}}') fixed no-repeat; background-size: cover;">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default margin-top-30">
                <div class="panel-heading text-center"><p>@lang($p."register")</p></div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                        {!! csrf_field() !!}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">@lang($p."name")</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" name="name" value="{{ old('name') }}">

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">@lang("auth/login.email")</label>

                            <div class="col-md-6">
                                <input type="email" class="form-control" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">@lang("auth/login.password")</label>

                            <div class="col-md-6">
                                <input type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">@lang($p."password_confirm")</label>

                            <div class="col-md-6">
                                <input type="password" class="form-control" name="password_confirmation">

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary width-100-percent">
                                    <i class="fa fa-btn fa-user"></i>@lang($p."register")
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
