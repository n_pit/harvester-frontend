<?php
    $ei = "common/editable_input.";
?>
<input class="new-value no-border" name="new_value" value="{{$value}}" type="text" autocomplete="off" maxlength="100">
{!! Form::token() !!}
<button type="button" class="done-btn">@lang($ei."done")</button>
<button type="button" class="cancel-btn">@lang($ei."cancel")</button>
