<?php
    $p = "landing_page.";
?>

@extends('layouts.front-layout')

@section('head')
    <title>@lang($p."landing_page_title")</title>
    <link href="{{ asset("css/forms/custom_form.css") }}" rel="stylesheet" type="text/css">
    <link href="{{ asset("select2/css/select2.min.css") }}" rel="stylesheet" type="text/css">
    <link href="{{ asset("css/plugins_custom_css/modal_custom.css") }}" rel="stylesheet" type="text/css">
    <link href="{{ asset("css/plugins_custom_css/select2_custom.css") }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('plugins/sweetalert-master/dist/sweetalert.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset("css/plugins_custom_css/sweetalert_custom.css") }}" rel="stylesheet" type="text/css">
    <link href="{{ asset("css/landing_page.css") }}" rel="stylesheet" type="text/css">
@stop

@section('content')

    {{-- Section 1 --}}
    <div class="search-keywords row padding-bottom-250 margin-top-minus-50" style="background: url('{{asset('imgs/town.jpeg')}}') fixed no-repeat; background-size: cover;">
        <div class="col-md-6 col-xs-6 col-md-offset-3 col-xs-offset-3 text-center margin-top-80">
            <h1>@lang($p."motto")</h1>
        </div>
        <div class="col-md-8 col-xs-8 col-md-offset-2 col-xs-offset-2 margin-top-20">
            <form action="{{ url('landing-page-search') }}" method="POST" class="select2-form">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="textarea-container row">
                    <label name="keyword[]"></label>
                    <div class="col-md-9 col-xs-12">
                        <select id="searchLead" name="keyword[]" class="js-select-search-tags width-100-percent display-inline pull-left"
                                multiple="multiple" data-placeholder="@lang($p."find_leads_placeholder")" style="width: 100%;"
                                data-error-msg="@lang("search_error_messages.search_input_error")"
                                data-popover-content="@lang($p."popover_content")"></select>
                        <div class="text-center margin-top-20">
                            <input type="radio" name="location_search" class="location-search" id="search-the-world" value="0" checked="checked">
                            <label for="search-the-world" class="font-size-1_2em">@lang($p."world_search")</label>
                            <input type="radio" name="location_search" class="location-search" id="search-country" value="1">
                            <label for="search-country" class="font-size-1_2em">@lang($p."select_country")</label>
                            <select name="country" disabled="disabled" class="font-size-1_2em">
                                <option value="gr" selected>Greece</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-12">
                        <button type="submit" id="find-leads-short-message" class="btn-lg btn-xs width-100-percent display-inline pull-left">@lang($p."find_leads")</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    {{-- Section 2 --}}
    <div class="row features padding-top-bottom-70">
        <div class="col-md-4 col-xs-4 text-center padding-left-right-50">
            <h1 class="bold"><i class="material-icons custom-material-icon">&#xE2C4;</i> @lang($p."get")</h1>
            <p>@lang($p.'get_text')</p>
        </div>
        <div class="col-md-4 col-xs-4 text-center padding-left-right-50">
            <h1 class="bold"><i class="material-icons custom-material-icon">&#xE16D;</i> @lang($p."prioritise")</h1>
            <p>@lang($p.'prioritise_text')</p>
        </div>
        <div class="col-md-4 col-xs-4 text-center padding-left-right-50">
            <h1 class="bold"><i class="material-icons custom-material-icon">&#xE0E0;</i> @lang($p."integrate")</h1>
            <p>@lang($p.'integrate_text')</p>
        </div>
    </div>

    {{-- Section 3 --}}
    <div class="row how-it-works padding-top-100">
        <div class="col-md-12 col-xs-12">
            <h1 class="padding-left-right-50 bold">@lang($p."how_works")</h1>
        </div>
    </div>

    <div class="row how-it-works padding-bottom-100">
        <div class="col-md-4 col-xs-4 col-lg-4 text-center padding-left-right-50">
            <div class="custom-box">
                <img src="{{asset("imgs/type_keywords.png")}}">
            </div>
            <div class="custom-circle">1</div>
            <div class="bold font-size-1_3em">
                <p>@lang($p."enter_keywords")</p>
            </div>
        </div>


        <div class="col-md-4 col-xs-4 col-lg-4 text-center padding-left-right-50">
            <div class="custom-box">
                <img src="{{asset("imgs/login_modal.png")}}">
            </div>
            <div class="custom-circle">2</div>
            <div class="bold font-size-1_3em">
                <p>@lang($p."login_text")</p>
            </div>
        </div>

        <div class="col-md-4 col-xs-4 col-lg-4 text-center padding-left-right-50">
            <div class="custom-box">
                <img src="{{asset("imgs/get_leads.png")}}">
            </div>
            <div class="custom-circle">3</div>
            <div class="bold font-size-1_3em">
                <p>@lang($p."get_leads")</p>
            </div>
        </div>
    </div>

    {{-- Section 4 --}}
    <div class="technology row padding-top-bottom-80" style="background: url('{{asset('imgs/paperwall.jpeg') }}') no-repeat fixed; background-size: cover;">
        <div class="col-md-12 col-xs-12">
            <div>
                <h1 class="padding-left-right-85 bold font-color-white">@lang($p."our_tech")</h1>
            </div>
            <div>
                <p class="padding-left-right-85 font-color-white font-size-1_8em">@lang($p.'our_tech_info')</p>
            </div>
        </div>
    </div>

    {{-- Section 5 --}}
    <div class="row padding-top-bottom-60">
        <div class="col-md-12 col-xs-12">
            <div class="padding-left-right-85">
                <h1 class="bold">@lang($p.'solutions')</h1>
            </div>

            <div class="padding-left-right-85 padding-bottom-20">
                <div class="col-md-10 col-sm-9 padding-none">
                    <p class="padding-bottom-20 font-size-1_8em">@lang($p.'solutions_info')</p>
                    <p class="font-size-1_8em">@lang($p.'solutions_info_2')</p>
                </div>
                <div class="col-md-2 col-sm-3 padding-none contact-wrapper margin-top-20">
                    <button class="contact-button" data-toggle="modal" data-target="#contact-modal">@lang($p.'contact')</button>
                </div>
            </div>
        </div>
    </div>

    @include('partials.modals.landing_page_login_modal')
    @include('partials.modals.contact_us_modal')

@stop

@section('scripts')
    <script src="{{asset('select2/js/select2_tokenize_fix.js')}}"></script>
    <script src="{{asset('plugins/sweetalert-master/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('js/messages/sweet_alert_error_msgs.js')}}"></script>
    <script src="{{asset('js/messages/sweet_alert_success_msgs.js')}}"></script>
    <script src="{{asset('js/searches/check_search_input.js')}}"></script>
    <script src="{{asset('js/searches/handle_country_selection_box.js')}}"></script>
    @if(\Auth::check())
    <script src="{{asset('js/searches/auth_landing_page_searches.js')}}"></script>
    @else
    <script src="{{asset('js/searches/guest_landing_page_searches.js')}}"></script>
    @endif

    <script type="text/javascript">
    var observe;
    if (window.attachEvent) {
        observe = function (element, event, handler) {
            element.attachEvent('on'+event, handler);
        };
    }
    else {
        observe = function (element, event, handler) {
            element.addEventListener(event, handler, false);
        };
    }
    function init () {
        var text = document.getElementById('text');
        function resize () {
            text.style.height = 'auto';
            text.style.height = text.scrollHeight+'px';
        }
        // 0-timeout to get the already changed text
        function delayedResize () {
            window.setTimeout(resize, 0);
        }
        observe(text, 'change',  resize);
        observe(text, 'cut',     delayedResize);
        observe(text, 'paste',   delayedResize);
        observe(text, 'drop',    delayedResize);
        observe(text, 'keydown', delayedResize);

        text.focus();
        text.select();
        resize();
    }
    </script>
    <script src="{{asset('js/check_email_validity.js')}}"></script>
    <script src="{{asset('js/landing_page.js')}}"></script>
    <script src="{{asset('js/searches/select2_input_validation.js')}}"></script>
@stop



