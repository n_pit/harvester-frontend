<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title')</title>

    {{-- bootstrap --}}
    <link href="{{ asset("plugins/bootstrap-3.3.6-dist/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css">

    {{-- icheck css --}}
    <link href="{{ asset("plugins/icheck-1.x/skins/square/blue.css") }}" rel="stylesheet" type="text/css">

    {{-- custom css --}}
    <link href="{{ asset("css/common.css") }}" rel="stylesheet" type="text/css">
    <link href="{{ asset("css/backoffice/panel.css") }}" rel="stylesheet" type="text/css">

    @yield('head')
</head>
<body data-url="{{ URL::to('/') }}" @if($displayMenu == "yes")class="menu-open"@endif>
        <div class="row">
            <div id="top-bar" class="col-md-12">
                @if($displayMenu == "yes")
                <a id="show-menu" class="pull-left" href="javascript:void(0)">
                    <span class="glyphicon glyphicon-menu-hamburger glyphicon-white"></span>
                </a>
                @endif
                <h4 id="logo" class="pull-left">@lang("landing_page.logo")</h4>
                <h4 class="pull-right">
                    <span class="dropdown">
                        <a id="dropdown-label" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="javascript:void(0)">
                            <span id="user-name">{{ \Auth::user()->name }}</span>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="dropdown-label">
                            <li><a href="{{ url('logout') }}"><span class="glyphicon glyphicon-log-out"></span>  Logout</a></li>
                        </ul>
                    </span>
                </h4>
            </div>
        </div>
        <div id="side-bar" class="padding-none">
            <div id="menu">
                <h6>@lang("backoffice/backoffice_panel.menu")</h6>
                <a class="menu-list-item"  href="{{ url('new-search') }}">
                    <div>
                        <h3 id="new_search" class="">@lang("backoffice/backoffice_panel.new")</h3>
                    </div>
                </a>
                <a class="menu-list-item" href="{{ url('display-searches') }}">
                    <div>
                        <h3 id="all_searches" class="">@lang("backoffice/backoffice_panel.all")</h3>
                    </div>
                </a>
            </div>
            <div class="scify-copyright-inner"><span class="font-size-17px">&#169; SCIFY {{date('Y')}}</span></div>
        </div>
        <div id="content" class="padding-none gray-background">
            @yield('content')

            <div id="footer" class="row margin-none">
                <div class="col-md-12">
                    <span><a href="#" class="font-size-15px">@lang("backoffice/backoffice_panel.terms")</a></span>
                    <span><a href="#" class="font-size-15px">@lang("backoffice/backoffice_panel.privacy")</a></span>
                </div>
            </div>
        </div>

    <!-- JavaScripts -->
    <script src="{{asset('js/jquery-2.1.4.min.js')}}"></script>
    <script src="{{asset('js/jquery-ui.min.js')}}"></script>
    <script src="{{asset('bootstrap-3.3.6/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('plugins/icheck-1.x/icheck.min.js')}}"></script>
    <script src="{{asset('js/common.js')}}"></script>
    <script src="{{asset('js/panel/panel.js')}}"></script>

    {{-- legacy IE support for transitions --}}
    <!--[if lte IE 9]>
        <script src="{{asset('js/legacy_ie_support/legacy_ie_support.js')}}"></script>
    <![endif]-->

    @yield('scripts')
</body>
</html>
