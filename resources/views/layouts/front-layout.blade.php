<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    {{-- fonts --}}
    <link href='https://fonts.googleapis.com/css?family=Josefin+Slab' rel='stylesheet' type='text/css'>
    {{--<link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel="stylesheet" type="text/css">--}}

    {{-- icons --}}
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="css/plugins_custom_css/material_icons_custom.css" rel="stylesheet">

    {{-- bootstrap --}}
    <link href="{{ asset("plugins/bootstrap-3.3.6-dist/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css">

    {{-- icheck css --}}
    <link href="{{ asset("plugins/icheck-1.x/skins/square/blue.css") }}" rel="stylesheet" type="text/css">

    {{-- custom css --}}
    <link href="{{ asset("css/common.css") }}" rel="stylesheet" type="text/css">

    @yield('head')

</head>
<body id="layout" data-url="{{ URL::to('/') }}">
    <div class="container-fluid height-100per">
        <?php
            $tabsSelection = array_fill(0, 2, "");
            if(isset($tabSelected)){
                if($tabSelected == "login"){
                    $tabsSelection[0] = "font-weight-600-imp";
                }
                if($tabSelected == "register"){
                    $tabsSelection[1] = "font-weight-600-imp";
                }
            }
        ?>
            <div class="row">
        <div id="top-bar" class="col-md-12">

                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#top-bar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="glyphicon glyphicon-menu-hamburger"></span>
                    </button>
                    <span class="navbar-brand padding-20"><a href="{{ url('/') }}">@lang("landing_page.logo")</a></span>
                </div>
                <div id="top-bar-collapse" class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        {{--<li><a href="#">@lang("landing_page.about")</a></li>--}}
                        @if (\Auth::check())
                        <li><a href="{{ url('display-searches') }}">@lang("landing_page.account")</a></li>
                        <li><a href="{{ url('logout') }}">@lang("landing_page.logout")</a></li>
                        @else
                        <li><a href="{{ url('login') }}" class="{{$tabsSelection[0]}}">@lang("landing_page.login")</a></li>
                        <li><a href="{{ url('register') }}" class="{{$tabsSelection[1]}}">@lang("landing_page.register")</a></li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
        @yield('content')

        <div class="row footer-back-color padding-top-bottom-13 footer-style footer">
            <div class="scify-copyright-outer col-md-2"><span class="font-size-17px">&#169; SCIFY {{date('Y')}}</span></div>
            <div class="col-md-3 col-md-offset-7 col-xs-12 terms-and-privacy-div text-right">
                <div class="col-md-6 col-md-offset-1">
                    <a href="#" class="font-size-15px">@lang("backoffice/backoffice_panel.terms")</a>
                </div>
                <div class="col-md-5">
                    <a href="#" class="font-size-15px">@lang("backoffice/backoffice_panel.privacy")</a>
                </div>
            </div>
        </div>

    </div>

    <!-- JavaScripts -->
    <script src="{{asset('js/jquery-2.1.4.min.js')}}"></script>
    <script src="{{asset('js/jquery-ui.min.js')}}"></script>
    <script src="{{asset('plugins/icheck-1.x/icheck.min.js')}}"></script>
    <script src="{{asset('js/common.js')}}"></script>
    <script src="{{asset('bootstrap-3.3.6/dist/js/bootstrap.min.js')}}"></script>
    @yield('scripts')
</body>
</html>
