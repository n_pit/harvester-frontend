{{-- Cancel Confirmation Modal --}}
<div class="modal fade" id="cancel-modal" tabindex="-1" role="dialog" aria-labelledby="CancelModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-body-title margin-bottom-20">
                    @lang('backoffice/my_searches.cancel_modal_title')
                </div>
                <div class="modal-text margin-bottom-20">
                    @lang('backoffice/my_searches.cancel_modal_text')
                </div>
            </div>
            <div class="modal-footer display-table width-100-percent">
                <div class="display-table-row">
                    <div class="text-link display-table-cell text-right vertical-align-middle width-62-percent padding-top-4">
                        <a href="javascript:void(0)" data-dismiss="modal" aria-label="Close">@lang('backoffice/my_searches.close')</a>
                    </div>
                    <div class="display-table-cell text-right">
                        <button class="cancel-modal-btn vertical-align-bottom">@lang('backoffice/my_searches.ok')</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
