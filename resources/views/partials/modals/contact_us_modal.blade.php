{{-- Contact us Modal --}}
<div class="modal fade" id="contact-modal" tabindex="-1" role="dialog" aria-labelledby="ContactModal"
    data-contact-us-url="{{url('contact-us')}}" data-contact-success-msg="@lang($p."contact_success_msg")"
    data-contact-error-msg="@lang($p."contact_error_msg")">
    <div class="modal-dialog" role="document">
        <div class="modal-content contact-modal-content">
            <div class="modal-body contact-modal-body">
                <div class="modal-body-title margin-bottom-20">
                    @lang('landing_page.contact')
                </div>
                <div class="contact-modal-text modal-text margin-bottom-30">
                    <div class="form-group">
                        {!! Form::text('contact_email', null, array('class' =>
                            'custom-text-input width-50-percent contact-placeholder',
                            'placeholder' => \Lang::get('landing_page.email_address'))) !!}
                        <span class="glyphicon glyphicon-ok email-validity color-green hide"></span>
                        <span class="glyphicon glyphicon-remove email-validity color-red hide"></span>
                    </div>
                    <div class="form-group width-50-percent">
                        {!! Form::text('subject', null, array('class' =>
                            'custom-text-input width-100-percent contact-placeholder',
                            'placeholder' => \Lang::get('landing_page.subject'))) !!}
                    </div>
                    <div class="form-group width-100-percent">
                        <textarea name="message" class="custom-textarea width-100-percent contact-placeholder position-relative"
                            placeholder="@lang('landing_page.message')" rows="5"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer contact-modal-footer display-table width-100-percent">
                <div class="display-table-row">
                    <div class="text-link display-table-cell text-left vertical-align-middle padding-top-4 padding-left-9">
                        <a href="javascript:void(0)" data-dismiss="modal" aria-label="Close">@lang('backoffice/display_search.cancel')</a>
                    </div>
                    <div class="display-table-cell text-right">
                        <button class="email-modal-btn btn btn-primary sign-in vertical-align-bottom" disabled="disabled">@lang('backoffice/display_search.send')</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
