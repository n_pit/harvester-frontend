{{-- Export to file Modal --}}
<div class="modal fade" id="export-modal" tabindex="-1" role="dialog" aria-labelledby="ExportModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-body-title margin-bottom-20">
                    @lang('backoffice/display_search.export_modal_title')
                </div>
                <div class="modal-text margin-bottom-30">
                    @lang('backoffice/display_search.export_modal_text')
                </div>
            </div>
            <div class="modal-footer display-table width-100-percent">
                <div class="display-table-row">
                    <div class="text-link display-table-cell text-left vertical-align-middle width-40-percent padding-top-4 padding-left-9">
                        <a href="javascript:void(0)" data-dismiss="modal" aria-label="Close">@lang('backoffice/display_search.cancel')</a>
                    </div>
                    <div class="display-table-cell text-right hide">
                        <button class="xls-btn export-btn btn btn-primary sign-in vertical-align-bottom">@lang('backoffice/display_search.xls')</button>
                    </div>
                    <div class="display-table-cell text-right">
                        <a href="{{url('export-contacts-csv')."/".$search['search_id']}}" class="csv-btn export-btn btn btn-primary sign-in vertical-align-bottom">@lang('backoffice/display_search.csv')</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
