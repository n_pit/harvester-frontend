{{-- Integrate Modal --}}
<div class="modal fade" id="integrate-modal" tabindex="-1" role="dialog" aria-labelledby="IntegrateModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-body-title margin-bottom-20">
                    @lang('backoffice/display_search.integrate_modal_title')
                </div>
                <div class="modal-text margin-bottom-30">
                    @lang('backoffice/display_search.integrate_modal_text')
                </div>
            </div>
            <div class="modal-footer display-table width-100-percent">
                <div class="display-table-row">
                    <div class="text-link display-table-cell text-left vertical-align-middle width-40-percent padding-top-4 padding-left-9">
                        <a href="javascript:void(0)" data-dismiss="modal" aria-label="Close">@lang('backoffice/display_search.cancel')</a>
                    </div>
                    <div class="display-table-cell text-right">
                        <a class="xls-btn integrate-btn btn btn-primary sign-in vertical-align-bottom" href="javascript:void(0)"><img class="height-100-percent width-100-percent" src="{{asset("imgs/logo_sugarcrm.png")}}"></a>
                    </div>
                    <div class="display-table-cell text-right">
                        <a class="csv-btn integrate-btn btn btn-primary sign-in vertical-align-bottom" href="javascript:void(0)"><img class="height-100-percent width-100-percent" src="{{asset("imgs/mailchimp_logo.png")}}"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
