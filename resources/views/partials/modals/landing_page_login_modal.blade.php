{{-- Login Modal --}}
<div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="LoginModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body text-center">
                <div class="modal-body-title">
                    @lang('landing_page.login_modal_title')
                </div>
                <div class="modal-text">
                    @lang('landing_page.login_modal_text')
                </div>
                <div class="padding-top-bottom-20 text-left col-md-6 col-md-offset-3 col-xs-8 col-xs-offset-2">
                    {!! Form::open(array('url' => url('login'))) !!}
                    <div class="form-group login-modal-label-div width-100-percent">
                        {!! Form::label('email', 'EMAIL') !!}
                        {!! Form::text('email', null, array('class' => 'custom-text-input width-100-percent')) !!}
                    </div>
                    <div class="form-group login-modal-label-div width-100-percent">
                        {!! Form::label('password', 'PASSWORD') !!}
                        {!! Form::password('password', array('class' => 'custom-text-input width-100-percent')) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer text-center display-table width-100-percent">
                <div class="display-table-row">
                    <div class="text-link display-table-cell text-left">
                        <a href="{{url('password/reset')}}">@lang('landing_page.forgot_password')</a>
                    </div>
                    <div class="display-table-cell">
                        <button type="submit" class="btn btn-primary sign-in vertical-align-bottom">@lang('landing_page.sign_in')</button>
                    </div>
                    <div class="text-link display-table-cell text-right">
                        <a href="{{url('register')}}">@lang('landing_page.no_account')</a>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
