{{-- Send email Modal --}}
<div class="modal fade" id="email-modal" tabindex="-1" role="dialog" aria-labelledby="EmailModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-body-title margin-bottom-20">
                    @lang('backoffice/display_search.email_modal_title')
                </div>
                <div class="modal-text margin-bottom-30">
                    <div class="form-group width-100-percent">
                        {!! Form::text('email', null, array('class' => 'custom-text-input width-100-percent email',
                            'placeholder' => \Lang::get('backoffice/display_search.email_address'))) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer display-table width-100-percent">
                <div class="display-table-row">
                    <div class="text-link display-table-cell text-left vertical-align-middle padding-top-4 padding-left-9">
                        <a href="javascript:void(0)" data-dismiss="modal" aria-label="Close">@lang('backoffice/display_search.cancel')</a>
                    </div>
                    <div class="display-table-cell text-right">
                        <button class="email-modal-btn btn btn-primary sign-in vertical-align-bottom">@lang('backoffice/display_search.send')</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
