<?php
    $p = "backoffice/my_searches.";
    // format correctly the dates!
    $datesHelper = new App\Services\DatesHelper();
?>

@extends('layouts.backoffice-layout')

@section('title')
    @lang("backoffice/backoffice_panel.all")
@endsection

@section('head')
    <link href="{{ asset('plugins/sweetalert-master/dist/sweetalert.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset("css/plugins_custom_css/sweetalert_custom.css") }}" rel="stylesheet" type="text/css">
    <link href="{{ asset("css/plugins_custom_css/modal_custom.css") }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/backoffice/all_searches.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('content')
    <div class="alert alert-warning alert-dismissible @if(isset($showSearchingAlert)){{$showSearchingAlert}}@else hide @endif" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        @lang($p."warning_text")
    </div>
    <div class="alert alert-danger alert-dismissible @if(isset($showSearchingError)){{$showSearchingError}}@else hide @endif" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        @lang($p."error_text")
    </div>
    <?php
        $arraySize = count($allSearches);
    ?>
    <div id="search_list" class="margin-bottom-45" data-searches-status-url="{{ url('user-status') }}" data-server-error-msg="@lang($p."server_problem")" data-completed="@lang($p."completed")">
        @foreach($allSearches as $key => $search)
        <div class="search-box" id="search_{{$search['search_id']}}" data-cancel-search-url="{{url("cancel-search" . "/" . $search['search_id'])}}">
            <div class="row row-height">
                <div class="col-md-9 col-height border-right-gray margin-right-10px">
                    <div class="list-search-div">
                        {{-- SEARCH TITLE --}}
                        <h1 class="search-header search-name-style margin-none">
                            <span class="enumerate-search">{{$key+1}}<span class="enumerate-dot">.</span></span>
                            <span class="searchAnchor current-value search-name-style no-border @if($search['search_status'] == 100) pointer @else inactive-link @endif" data-url="{{ url('display-search') . '/' . $search['search_id'] }}">
                                {{$search['search_name'] }}
                            </span>
                            <span class="edit-value" style="display: none;" data-edit-url="{{url('edit-search')}}"
                                data-edit-id="{{ $search['search_id'] }}">
                                @include('common.editable_input', ['value' => $search["search_name"]])
                            </span>
                            <span class="click-to-edit-value link-for-action pointer @if($search['search_status'] != 100) hide @endif">
                                <i class="glyphicon glyphicon-pencil"></i> @lang($p."rename")
                            </span>
                            <span class="click-to-cancel link-for-action pointer @if($search['search_status'] == 100) hide @endif">
                                <i class="glyphicon glyphicon-remove"></i> @lang($p."cancel")
                            </span>
                        </h1>
                        {{--<input type="hidden" name="search_id" value="{{ $search['search_id'] }}">--}}
                        <div class="search-keywords">
                            {{-- KEYWORDS --}}
                            <div class="row margin-bottom-20px">
                                <div class="col-md-12">
                                    <span class="keywords-font">
                                        <?php
                                            if(isset($search['keywords']) and !empty($search['keywords'])){
                                                $array_size = count($search['keywords']);
                                                $i = 0;
                                                foreach($search['keywords'] as $keyword){
                                                    echo $keyword;
                                                    if($i < $array_size - 1){
                                                        echo ", ";
                                                    }
                                                    $i++;
                                                }
                                            }
                                        ?>
                                    </span>
                                </div>
                            </div>
                            {{-- DATE --}}
                            <div class="row margin-top-60">
                                <div class="col-md-12">
                                    <span class="font-size-13px">
                                        @lang($p.'created_on'){{$datesHelper->getDateStringFromDBDate($search['search_start_date'])}}
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-height result-box @if($search['search_status'] == 100) pointer completed-box @else running-box @endif">
                    <div class="search-keywords">
                        {{-- PROGRESS BAR --}}
                        <div class="row">
                            <div class="col-md-12 text-align-center">
                                <div class="margin-top-minus-15 completed @if($search['got_contacts_found'] != 1) hide @endif">
                                    <div class="search-results-counter">{{$search['contacts_found_number']}}</div>
                                    <div class="font-size-20pxs">@lang($p."contacts_found")</div>
                                </div>
                                <div class="margin-top-20 running @if($search['search_status'] == 100 and $search['got_contacts_found'] == 1) hide @endif">
                                    <span class="in-progress counter-info">@if($search['search_status'] != 100)@lang($p.'running')@else @lang($p."completed")@endif</span>
                                    <div class="progress progress-bar-border progress-bar-style">
                                        <div class="progress-bar custom-bar-text" role="progressbar"
                                             aria-valuenow="{{$search['search_status']}}" aria-valuemin="0"
                                             aria-valuemax="100" style="width: {{$search['search_status']}}%;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>

    @include('partials.modals.cancel_confirmation_modal')

@endsection

@section('scripts')
    <script src="{{asset('plugins/sweetalert-master/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('js/messages/sweet_alert_error_msgs.js')}}"></script>
    <script src="{{asset('js/edit/editable_view.js')}}"></script>
    <script src="{{asset('js/all_searches.js')}}"></script>
@stop
