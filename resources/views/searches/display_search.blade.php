<?php
    $p = "backoffice/display_search.";
    // format correctly the dates!
    $datesHelper = new App\Services\DatesHelper();
?>

@extends('layouts.backoffice-layout')

@section('title')@lang("backoffice/backoffice_panel.display_search")@endsection

@section('head')
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/DataTables/datatables.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/DataTables/DataTables-1.10.10/css/jquery.dataTables.min.css') }}">
    <link href="{{ asset("css/plugins_custom_css/modal_custom.css") }}" rel="stylesheet" type="text/css">
    <link href="{{ asset("css/forms/custom_form.css") }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('plugins/sweetalert-master/dist/sweetalert.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset("css/plugins_custom_css/sweetalert_custom.css") }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('plugins/intro.js-master/minified/introjs.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset("css/plugins_custom_css/introjs_custom.css") }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/backoffice/all_searches.css') }}" rel="stylesheet" type="text/css">

    {{-- icons --}}
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="{{ asset('css/plugins_custom_css/material_icons_custom.css') }}" rel="stylesheet">

@endsection

@section('content')
<div class="search-box" data-delete-url="{{url("delete-search" . "/" . $search['search_id'])}}" data-store-contacts-url="{{ url('get-contacts') . '/' . $search['search_id'] }}" data-contacts-problem-msg="@lang($p."get_contacts_problem")">
    <div class="border-bottom-blue">
        <div class="display-search-div row">
                <div class="col-md-11 col-xs-11">
                    <h1 class="search-name-style margin-bottom-none margin-top-5">
                        <span class="current-value">@if(isset($search['search_name'])){{$search['search_name']}}@endif</span>
                        <span class="edit-value" style="display: none;" data-edit-url="{{url('edit-search')}}"
                              data-edit-id="{{ $search['search_id'] }}">
                            @include('common.editable_input', ['value' => $search["search_name"]])
                        </span>
                        <span class="click-to-edit-value link-for-action pointer">
                            <i class=" glyphicon glyphicon-pencil"></i><span class="action-name"> @lang($p."rename")</span>
                        </span>
                        <span class="link-for-action pointer delete-btn margin-left-20" data-msg-error-occurred="@lang($p."error_occurred")"><i class="glyphicon glyphicon-trash"></i><span class="action-name"> @lang($p."delete")</span></span>
                    </h1>
                </div>
                <div class="col-md-1 col-xs-1">
                    <span class="close close-single-search pull-right" data-redirect-url="{{url('display-searches')}}">×</span>
                </div>
            <div class="created-on col-md-12 col-xs-12">
                <span>@lang("backoffice/my_searches.created_on"){{$datesHelper->getDateStringFromDBDate($search['search_start_date'])}}</span>
            </div>
        </div>
    </div>
    <div class="display-search-div">
        <div class="row margin-top-20">
            <div class="info-div col-md-12 col-xs-12">
                <div class="search-results-info col-md-3 text-center padding-right-50">
                    <p class="counter-info">@lang("backoffice/display_search.contacts_found")</p>
                    <p class="search-results-counter">{{count($search['results'])}}</p>
                </div>
                <div class="keywords-used col-md-9" data-intro="@lang($p."press_keyword")" >
                    <p class="counter-info">@lang($p."keywords")</p>
                    <p class="keywords-list cyan" data-intro="@lang($p."remove_keyword")">
                        <?php
                            if(isset($search['keywords']) and !empty($search['keywords'])){
                                $array_size = count($search['keywords']);
                                $i = 0;
                                foreach($search['keywords'] as $key => $keyword){
                                    echo "<a href='javascript:void(0)' class='keyword cyan' data-keyword-id='" . $key .
                                            "'>" . $keyword . "</a>";
                                    if($i < $array_size - 1){
                                        echo ", ";
                                    }
                                    $i++;
                                }
                            }
                        ?>
                    </p>
                    <p class="selected-keywords-count" data-multiple-keywords="@lang($p."multiple_keywords")"
                        data-single-keyword="@lang($p."single_keyword")"
                        data-selected-keywords-text="@lang($p."selected_keywords_text")"
                        data-remove-all="@lang($p."remove_all")" style="opacity: 0;">1</p>
                </div>
            </div>
        </div>
    </div>
    <div class="options-div margin-bottom-10">
        <div class="row">
            {{--<div class="col-md-5 col-md-offset-7 col-sm-4 col-sm-offset-5 col-xs-4 col-xs-offset-4">--}}
            <div class="col-md-5 col-md-offset-7 col-xs-12">
                <div class="email col-md-4 hide">
                    <a href="javascript:void(0)"><i class="material-icons custom-material-icon dark-blue">&#xE0BE;</i> @lang($p."send_email")</a>
                </div>
                <div class="export col-md-12 text-right">
                    <a href="javascript:void(0)" class="padding-right-30"><i class="material-icons custom-material-icon dark-blue">&#xE0E2;</i> @lang($p."export")</a>
                </div>
                <div class="integrate col-md-4 hide">
                    <a href="javascript:void(0)"><i class="material-icons custom-material-icon dark-blue">&#xE0C3;</i> @lang($p."integrate")</a>
                </div>
            </div>
        </div>
    </div>
    <div class="search-results margin-top-30 margin-bottom-45" data->
        <div class="row padding-bottom-20">
            <div class="col-md-12 table-responsive padding-none padding-bottom-20">
                <table id="results-table">
                    <thead>
                        <th class="border-right">#</th>
                        <th class="border-right">@lang($p."company_name")</th>
                        <th class="border-right">@lang($p."email")</th>
                        <th class="border-right">@lang($p."contact_info")</th>
                        <th class="border-right">@lang($p."address")</th>
                        <th class="border-right">@lang($p."social_media")</th>
                        <th class="border-right hide">@lang($p."country")</th>
                        <th class="border-right hide">@lang($p."fb_followers")</th>
                        <th class="border-right hide">@lang($p."tw_followers")</th>
                        <th class="border-right hide">@lang($p."fb_ranking")</th>
                        <th class="border-right hide">@lang($p."source")</th>
                        <th>@lang($p."remove")</th>
                        <th class="hide">Related Queries</th>
                    </thead>
                    <tbody>
                <?php

                    if(isset($search['results']) and !empty($search['results'])){
                        foreach($search['results'] as $i => $result){
                            $should_put_bottom_border = $i - count($search['results']) - 1;
                            echo is_bool($should_put_bottom_border);
                            if($should_put_bottom_border > 0)echo "<tr class=\"bottom-border\">";else
                            echo "<tr>";
                            echo "<td class=\"border-right border-top\">" . ($i + 1) . "</td>";
                            echo "<td class=\"border-right border-top\">" . $result->formatDomain() . "</td>";
                            echo "<td class=\"border-right border-top\"><a href='mailto:" . $result->getEmail() .
                                    "'>" . $result->getEmail() . "</a></td>";
                            echo "<td class=\"border-right border-top\">" . $result->getPhone() . "</td>";
                            echo "<td class=\"border-right border-top\">" . $result->getAddress() . "</td>";
                            echo "<td class=\"border-right border-top\">" . $result->formatSocialMediaWithLinks() . "</td>";
                            echo "<td class=\"border-right border-top hide\"></td>";
                            echo "<td class=\"border-right border-top hide\"></td>";
                            echo "<td class=\"border-right border-top hide\"></td>";
                            echo "<td class=\"border-right border-top hide\"></td>";
                            echo "<td class=\"border-right border-top hide\"></td>";
                            echo "<td class=\"border-top\"><button class=\"btn btn-sm remove-contact-btn\"" .
                                    " data-toggle=\"modal\" data-target=\"#remove-contact-modal\"" .
                                    " data-remove-contact-url=\"" . url("remove-contact") . "/" . $result->getId() .
                                    "\">" . \Lang::get($p."remove_contact") . "</button></td>";
                            echo "<td class=\"hide\">" . $result->getRelatedQueries() . "</td>";
                            echo "</tr>";
                        }
                    }
                ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@include('partials.modals.delete_confirmation_modal')
@include('partials.modals.send_email_modal')
@include('partials.modals.export_to_file_modal')
@include('partials.modals.integrate_modal')
@include('partials.modals.remove_contact_confirmation_modal')

@endsection

@section('scripts')
    <script src="{{asset('js/edit/editable_view.js')}}"></script>
    <script src="{{asset('plugins/sweetalert-master/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('js/messages/sweet_alert_error_msgs.js')}}"></script>
    <script src="{{asset('plugins/intro.js-master/minified/intro.min.js')}}"></script>
    <script src="{{asset('js/display_search.js')}}"></script>
    {{--<script type="text/javascript" src="{{asset('plugins/DataTables/datatables.min.js')}}"></script>--}}
    <script src="{{asset('plugins/DataTables/DataTables-1.10.10/js/jquery.dataTables.min.js')}}"></script>
    {{--@if($search['got_contacts_found'] == 0 and $search['search_status'] == 100)--}}
    {{--<script src="{{asset('js/searches/get_contacts_from_search.js')}}"></script>--}}
    {{--@endif--}}
@endsection
