<?php
    $p = "backoffice/new-search.";
?>

@extends('layouts.backoffice-layout')

@section('title')
    @lang($p.'new-search')
@stop

@section('head')
    <link href="{{ asset('select2/css/select2.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset("css/plugins_custom_css/select2_custom.css") }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('plugins/sweetalert-master/dist/sweetalert.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset("css/plugins_custom_css/sweetalert_custom.css") }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/backoffice/new_search.css') }}" rel="stylesheet" type="text/css">
@stop

@section('content')
    <div class="new-search-border no-bottom-border ">
        {{-- TABS BUTTONS --}}
         {{--<ul class="nav nav-tabs background-color-grey">--}}
          {{--<li class="active li-background-color-dark-grey shadow margin-right-10px width-18per text-align-center padding-top-bottom-5px">--}}
              {{--<a data-toggle="tab" class="no-margin tab-font-style" href="#keywords">@lang($p.'keywords')</a>--}}
          {{--</li>--}}
          {{--<li class="shadow li-background-color-dark-grey width-18per text-align-center padding-top-bottom-5px">--}}
              {{--<a data-toggle="tab" class="no-margin tab-font-style" href="#urls">@lang($p.'urls')</a>--}}
          {{--</li>--}}
        {{--</ul>--}}

        {{-- TABS --}}
        <div class="tab-content shadow margin-bottom-50">
            {{-- KEYWORDS--}}
            <div id="keywords" class="tab-pane fade in active tab-container-padding"
                data-popover-content="@lang($p."keywords_popover_content")">
                <h1 class="tab-header-font">@lang($p.'keywords_title')</h1>
                <p class="text-below-title">@lang($p.'keywords_text')</p>

                <form action="{{ url('new-search/search-from-keywords') }}" method="POST" class="search-from-keywords select2-form">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="textarea-container">
                        <div class="row">
                            <div class="col-md-9 col-xs-12">
                                <select name="keywords[]" class="searchLead js-select-search-keywords pull-left js-select-search-tags" multiple="multiple" style="width: 100%" data-placeholder="@lang($p."keywords_placeholder")" data-error-msg="@lang("search_error_messages.search_input_error")"></select>
                                <div class="text-center margin-top-20">
                                    <input type="radio" name="location_search" class="location-search" id="search-the-world" value="0" checked="checked">
                                    <label for="search-the-world" class="font-size-1_2em">@lang("landing_page.world_search")</label>
                                    <input type="radio" name="location_search" class="location-search" id="search-country" value="1">
                                    <label for="search-country" class="font-size-1_2em">@lang("landing_page.select_country")</label>
                                    <select name="country" disabled="disabled" class="font-size-1_2em">
                                        <option value="gr" selected>Greece</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-12">
                                <button type="submit" id="find-leads" class="btn-lg btn-xs width-100-percent pull-left">@lang($p."find_leads")</button>
                            </div>
                        </div>
                    </div>
                </form>

            </div>
            {{-- URLs --}}
            <div id="urls" class="tab-pane fade tab-container-padding">
                <h1 class="tab-header-font">@lang($p.'urls-text-1')</h1>
                <p class="text-below-title">@lang($p.'urls-text-2')</p>

                <form action="{{ url('new-search/search-from-urls') }}" method="POST" class="search-from-urls margin-bottom-80px">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="textarea-container ">
                        <div class="row">
                            <div class="col-lg-10">
                                <select name="urls[]" class="searchLead js-select-search-urls custom-input-text width-80-percent pull-left" multiple="multiple" style="width: 100%"></select>
                            </div>
                            <div class="col-lg-2">
                                <button  type="submit" class="find-leads-short-message btn-lg btn-xs width-100-percent pull-right submit-button">@lang($p."find_leads")</button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-10">
                                <button type="submit" class="find-leads-long-message btn-lg btn-xs width-19-percent pull-left submit-button margin-top-20px">@lang($p."find_leads")</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop


@section('scripts')
    <script src="{{asset('select2/js/select2_tokenize_fix.js')}}"></script>
    <script src="{{asset('js/new_search.js')}}"></script>
    <script src="{{asset('plugins/sweetalert-master/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('js/messages/sweet_alert_error_msgs.js')}}"></script>
    <script src="{{asset('js/searches/check_search_input.js')}}"></script>
    <script src="{{asset('js/searches/handle_country_selection_box.js')}}"></script>
    <script src="{{asset('js/searches/select2_input_validation.js')}}"></script>
@stop
